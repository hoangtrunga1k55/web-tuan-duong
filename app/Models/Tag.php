<?php

namespace App\Models;

class Tag extends MongoModel
{
    const STATUS = 1;
    protected $collection = 'tags';
    protected $guarded = [];

    public function news()
    {
        return $this->belongsToMany(Post::class);
    }
}
