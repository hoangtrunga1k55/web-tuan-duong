<?php

namespace App\Models;

class Contact extends MongoModel
{
    protected $collection = 'contacts';
    protected $guarded = [];
}
