<?php

namespace App\Models;

use App\Repositories\Contracts\HomepageRepositoryInterface;
use Illuminate\Support\Facades\DB;

class Homepage extends MongoModel
{
    protected $guarded = [];

    public function posts()
    {
        return DB::table('posts')->whereIn('_id', $this->post_ids ?? [])->get();
    }
    public function category(){
        return $this->belongsTo(Category::class);
    }
}
