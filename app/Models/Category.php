<?php

namespace App\Models;

class Category extends MongoModel
{
    const STATUS = 1;
    const TYPE = [
        0 => "Product",
        1 => "News"
    ];
    const TYPE_PRODUCT = 0;
    const TYPE_NEWS = 1;
    protected $collection = 'category';
    protected $guarded = [];

    /**
     * @var mixed
     */

    public function getID(): string
    {
        return $this->id ?? "";
    }

    public function getNewsIDs(): array
    {
        if (!is_array($this->news_ids)) {
            return [];
        };
        return $this->news_ids ?? [];
    }


    public function news()
    {
        return $this->belongsToMany(Post::class);
    }

    public function getChildNav()
    {
        return self::query()
            ->where('parent_id', 'like', $this->_id . ",")
            ->where('show', 'like', 1)
            ->get();
    }
}
