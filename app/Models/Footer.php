<?php

namespace App\Models;

class Footer extends MongoModel
{
    protected $guarded = [];

    function getID(): string
    {
        return $this->_id ?? "";
    }
}
