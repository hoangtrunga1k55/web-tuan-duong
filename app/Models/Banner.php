<?php

namespace App\Models;

class Banner extends MongoModel
{
    protected $collection = 'banners';

    protected $guarded = [];

    function getID(): string
    {
        return isset($this->_id) ? $this->_id : "";
    }
}
