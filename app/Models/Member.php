<?php

namespace App\Models;

class Member extends MongoModel
{
    protected $collection = 'members';

    protected $guarded = [];

}
