<?php

namespace App\Models;

use App\Models\Member;

class Notification extends MongoModel
{
    protected $collection = 'notifications';
    protected $guarded = [];

    public function user(){
        return $this->belongsTo(Member::class, 'user_id');
    }
}
