<?php

namespace App\Models;

class Header extends MongoModel
{
    protected $collection = 'headers';

    protected $guarded = [];

    function getID(): string
    {
        return $this->_id ?? "";
    }
}
