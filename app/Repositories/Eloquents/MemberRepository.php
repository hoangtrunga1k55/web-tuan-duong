<?php

namespace App\Repositories\Eloquents;

use App\Models\Member;
use App\Repositories\Contracts\MemberRepositoryInterface;

class MemberRepository extends BaseRepository implements MemberRepositoryInterface
{
    function __construct(Member $model)
    {
        $this->model = $model;
    }
    public function getModel()
    {
        return Member::class;
    }
}
