<?php

namespace App\Repositories\Eloquents;

use App\Models\Homepage;
use App\Repositories\Contracts\HomepageRepositoryInterface;

class HomepageRepository extends BaseRepository implements HomepageRepositoryInterface
{
    function __construct(Homepage $model)
    {
        $this->model = $model;
    }
    public function getModel()
    {
        return Homepage::class;
    }
}
