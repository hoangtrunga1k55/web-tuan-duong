<?php

namespace App\Repositories\Eloquents;

use App\Models\Banner;
use App\Repositories\Contracts\BannerRepositoryInterface;

class BannerRepository extends BaseRepository implements BannerRepositoryInterface
{
    function __construct(Banner $model)
    {
        $this->model = $model;
    }
    public function getModel()
    {
        return Banner::class;
    }
}
