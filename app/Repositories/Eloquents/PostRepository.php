<?php

namespace App\Repositories\Eloquents;

use App\Models\Category;
use App\Models\Post;
use App\Repositories\Contracts\PostRepositoryInterface;

class PostRepository extends BaseRepository implements PostRepositoryInterface
{
    function __construct(Post $model)
    {
        $this->model = $model;
    }
}
