<?php

namespace App\Repositories\Eloquents;

use App\Models\Footer;
use App\Repositories\Contracts\FooterRepositoryInterface;

class FooterRepository extends BaseRepository implements FooterRepositoryInterface
{
    function __construct(Footer $model)
    {
        $this->model = $model;
    }
    public function getModel()
    {
        return Footer::class;
    }
}
