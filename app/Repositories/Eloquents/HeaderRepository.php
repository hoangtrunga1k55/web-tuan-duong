<?php

namespace App\Repositories\Eloquents;

use App\Models\Banner;
use App\Models\Header;
use App\Repositories\Contracts\BannerRepositoryInterface;
use App\Repositories\Contracts\HeaderRepositoryInterface;

class HeaderRepository extends BaseRepository implements HeaderRepositoryInterface
{
    function __construct(Header $model)
    {
        $this->model = $model;
    }
    public function getModel()
    {
        return Header::class;
    }
}
