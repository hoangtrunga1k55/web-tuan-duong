<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\DefaultLanguage;
use App\Http\Requests\NewsRequest;
use App\Models\Post;
use App\Repositories\Contracts\CategoryRepositoryInterface;
use App\Repositories\Contracts\CmsAccountRepositoryInterface;
use App\Repositories\Contracts\LanguageRepositoryInterface;
use App\Repositories\Contracts\PostRepositoryInterface;
use App\Repositories\Contracts\TagRepositoryInterface;
use App\Repositories\Contracts\VendorRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\ImageHelper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class PostController extends Controller
{

    protected $postRepository;
    protected $categoryRepository;
    protected $cmsAccountRepository;
    protected $tagRepository;
    protected $vendorRepository;
    protected $languageRepository;

    public function __construct(
        PostRepositoryInterface $postRepository,
        CategoryRepositoryInterface $categoryRepository,
        CmsAccountRepositoryInterface $cmsAccountRepository,
        VendorRepositoryInterface $vendorRepository,
        TagRepositoryInterface $tagRepository,
        LanguageRepositoryInterface $languageRepository
    )
    {
        $this->postRepository = $postRepository;
        $this->categoryRepository = $categoryRepository;
        $this->cmsAccountRepository = $cmsAccountRepository;
        $this->tagRepository = $tagRepository;
        $this->vendorRepository = $vendorRepository;
        $this->languageRepository = $languageRepository;
    }


    public function index(Request $request)
    {
        $conditions = [];
        if (!is_null($request->title)) {
            $conditions['where'][] = ['title', 'like', "%" . $request->title . "%"];
        }
        if (!is_null($request->category)) {
            $conditions['where'][] = ['category_ids', 'all', [$request->category]];
        }
        if (!is_null($request->name)) {
            $condition = [];
            $condition['where'][] = ['full_name', 'like', "%" . $request->name . "%"];

            $creators = $this->cmsAccountRepository->all(array('*'), $condition);
            $creator_ids = array_column($creators->toArray(), '_id');
            $conditions['whereIn']['creator_id'] = $creator_ids;
        }
        if (!is_null($request->status)) {
            $conditions['where'][] = ['status', 'like', $request->status];
        }
        if (!is_null($request->created_form)) {
            $conditions['where'][] = ['created_at', '>=', Carbon::createFromFormat('Y-m-d\TH:i', $request->created_from)];
        }
        if (!is_null($request->created_to)) {
            $conditions['where'][] = ['created_at', '<=', Carbon::createFromFormat('Y-m-d\TH:i', $request->created_to)];
        }
        if (!is_null($request->status)) {
            $conditions['where'][] = ['status', 'like', $request->status];
        }
        if (!is_null($request->approved_from)) {
            $conditions['where'][] = [
                'approved_at',
                '>=',
                Carbon::createFromFormat('Y-m-d\TH:i', $request->approved_from)
            ];
        }
        if (!is_null($request->approved_to)) {
            $conditions['where'][] = [
                'approved_at',
                '<=',
                Carbon::createFromFormat('Y-m-d\TH:i', $request->approved_to)
            ];
        }
        if (!is_null($request->published_from)) {
            $conditions['where'][] = [
                'published_at',
                '>=',
                Carbon::createFromFormat('Y-m-d\TH:i', $request->published_from)
            ];
        }
        if (!is_null($request->published_to)) {
            $conditions['where'][] = [
                'published_at',
                '<=',
                Carbon::createFromFormat('Y-m-d\TH:i', $request->published_to)
            ];
        }
        if (!is_null($request->lang_type)) {
            $conditions['where'][] = ['lang_type', 'like', "%" . $request->lang_type . "%"];
        }

        $posts = $this->postRepository->paginateList(10, $conditions);
        $categories = $this->categoryRepository->all();
        $cmsAccounts = $this->cmsAccountRepository->all();
        $tags = $this->tagRepository->all();
        $languages = $this->languageRepository->all(['*'],['where'=>[['status','like','1']]]);
        return view('admin.pages.post.list', compact(
            'posts',
            'categories',
            'cmsAccounts',
            'request',
            'tags',
            'languages'
        ));
    }

    public function create()
    {
        $categories = $this->categoryRepository->all();
        $tags = $this->tagRepository->all();
        $vendors = $this->vendorRepository->all();
        $languages = $this->languageRepository->all();
        return view('admin.pages.post.form', compact(
            'categories',
            'tags',
            'vendors', 'languages'
        ));
    }

    public function store(NewsRequest $request)
    {
        $this->handleSubmitRequest($request);

        return response()->json([
            'message' => 'Thêm mới tin tức thành công',
            'status' => 'success',
            'url' => route('admin.post.index')
        ]);
    }

    public function show(Request $request)
    {

        $post = $this->postRepository->findById($request->id);
        $post->full_name = $post->author->full_name;
        if (!is_null($post->approve)) {
            $post->approve_by = $post->approve->full_name;
        }
        $categories = "";
        foreach ($post->categories as $category) {
            $categories .= $category->title;
        }
        $post->url_for_client = route('site.post.detail', $post->slug);
        $post->categories_name = $categories;
        if (isset($post->approved_at) && !is_null($post->approved_at)) {
            $post->approved_at = Carbon::parse($post->approved_at)->format('H:i d-m-Y');
        }
        if (isset($post->published_at) && !is_null($post->published_at)) {
            $post->published_at = Carbon::parse($post->published_at)->format('H:i d-m-Y');
        }
        if (isset($post->created_at) && !is_null($post->created_at)) {
            $post->formated_created_at = Carbon::parse($post->created_at->diffForHumans())->format('H:i d-m-Y');
            $post->diff = $post->created_at->locale('vi')->diffForHumans(Carbon::now());
        }

        return response()->json([
            'post' => $post
        ]);
    }

    public function edit($id)
    {
        $post = $this->postRepository->findById($id);
        $categories = $this->categoryRepository->all();
        $tags = $this->tagRepository->all();
        $vendors = $this->vendorRepository->all();
        $languages = $this->languageRepository->all();
        return view('admin.pages.post.form', compact(
            'post',
            'categories',
            'tags',
            'vendors',
            'languages'
        ));
    }

    public function update(NewsRequest $request, $id)
    {
        $this->handleSubmitRequest($request, $id);

        return response()->json([
            'message' => 'Sửa thành công',
            'status' => 'success',
            'url' => route('admin.post.index')
        ]);
    }

    public function destroy($id)
    {
        $this->postRepository->destroy($id);

        return response()->json([
            'message' => 'Xóa thành công',
            'status' => 'success'
        ]);
    }

    public function updateStatus(Request $request)
    {
        $attributes = [];
        $item = $this->postRepository->findById($request->id);

        $attributes['status'] = $request->status;
        switch ($request->status) {
            case Post::WAIT_REVIEW:
                $attributes['approved_at'] = null;
                $attributes['approved_by'] = null;
                break;
            case Post::WAIT_RELEASE:
                $attributes['approved_at'] = Carbon::now();
                $attributes['approved_by'] = Auth::id();
                break;
            case Post::RELEASE:
                if (is_null($item->approved_at)) {
                    $attributes['approved_at'] = Carbon::now();
                    $attributes['approved_by'] = Auth::id();
                }
                if (is_null($item->published_at)) {
                    $attributes['published_at'] = Carbon::now();
                    $attributes['published_by'] = Auth::id();
                }
                break;
        }

        $item->fill($attributes)->save();

        return response()->json([
            'message' => 'Cập nhật trạng thái thành công',
            'status' => 'success'
        ]);
    }

    public function approve($id)
    {
        $item = $this->postRepository->findById($id);

        if ($item->status != Post::WAIT_REVIEW) {
            return redirect()->route('admin.post.index')
                ->with('message', 'Bài viết này chưa hoàn thành hoặc đã được công bố rồi');
        }
        $data = [];

        $data['status'] = Post::WAIT_RELEASE;

        $item->fill($data)->save();

        return redirect()->route('admin.post.index')
            ->with('message', 'Duyệt bài viết thành công');
    }

    public function publish($id)
    {
        $item = $this->postRepository->findById($id);
        if ($item->status != Post::WAIT_RELEASE) {
            return redirect()->route('admin.post.index')
                ->with('message', 'Bài viết này chưa được duyệt hoặc đã được công bố rồi');
        }
        $data = [];

        if (is_null($item->published_at)) {
            $data['published_at'] = Carbon::now();
        }
        $data['status'] = Post::RELEASE;

        $item->fill($data)->save();

        return redirect()->route('admin.post.index')
            ->with('message', 'Công bố bài viết thành công');
    }

    public function checkTag($tagIds)
    {
        $originTasks = $this->tagRepository->all();
        $_ids = array_column($originTasks->toArray(), '_id');
        foreach ($tagIds as $key => $tagId) {
            if (!in_array($tagId, $_ids)) {
                $data['_id'] = (string)Str::uuid();
                $data['name'] = $tagId;
                $data['slug'] = Str::slug($tagId);
                $data['description'] = $tagId;
                $data['status'] = 1;
                $this->tagRepository->insert($data);
                unset($tagIds[$key]);
                array_push($tagIds, $data['_id']);
            }
        }
        return $tagIds;
    }

    public function handleSubmitRequest($request, $id = null)
    {
        $attributes = $request->validated();
        $attributes['slug'] = Str::slug($attributes['title']);
        $category_ids = [];
        $tag_ids_after = [];
        $vendor_ids = [];
        if (isset($attributes['published_at'])) {
            $attributes['published_at'] =
                Carbon::createFromFormat('Y-m-d\TH:i', $request->published_at);
        }
        if (isset($attributes['category_ids'])) {
            $category_ids = $attributes['category_ids'];
            unset($attributes['category_ids']);
        }
        if (isset($attributes['tag_ids'])) {
            $tag_ids = $attributes['tag_ids'];
            $tag_ids_after = $this->checkTag($tag_ids);
        }
        if (isset($attributes['vendor_ids'])) {
            $vendor_ids = $attributes['vendor_ids'];
            unset($attributes['vendor_ids']);
        }

        if (!is_null($id)) {
            $item = $this->postRepository->findById($id);
            if (!isset($item)) {
                return redirect()->route('admin.post.index')
                    ->with('message', 'Bản ghi này đã bị xóa khỏi hệ thống');
            }
            $item->fill($attributes)
                ->save();

            $item->tags()
                ->sync($tag_ids_after);

            $item->categories()
                ->sync($category_ids);

            $item->vendors()
                ->sync($vendor_ids);


            if ($item->status == Post::WAIT_RELEASE || $item->status == Post::RELEASE) {
                $attributes['approved_at'] = null;
                $attributes['approved_by'] = null;
                $item->status = (int)Post::WAIT_REVIEW;
                $item->save();
            }
        } else {
            $attributes['status'] = Post::WAIT_REVIEW;
            $attributes['creator_id'] = Auth::id();

            $post = $this->postRepository->create($attributes);
            $post->categories()->sync($category_ids);
            $post->tags()->sync($tag_ids_after);
            $post->vendors()->sync($vendor_ids);
        }
    }
}
