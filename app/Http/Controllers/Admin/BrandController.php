<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ImageHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\BrandRequest;
use App\Repositories\Contracts\BrandRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class BrandController extends Controller
{
    protected $brandRepository;

    public function __construct(BrandRepositoryInterface $brandRepository)
    {
        $this->brandRepository = $brandRepository;
    }


    public function index()
    {
        $brands = $this->brandRepository->paginateList();

        return view('admin.pages.brand.index', compact(
            'brands'
        ));
    }

    public function create()
    {
        return view('admin.pages.brand.form');
    }


    public function store(BrandRequest $request)
    {
        $this->handleSubmitRequest($request);

        return response()->json([
            'message' => 'Thêm thành công',
            'status' => 'success',
            'url' => route('admin.brand.index')
        ]);
    }

    public function edit($id)
    {
        $brand = $this->brandRepository->findById($id);

        if (empty($brand)) return redirect()->route('admin.brand.index');

        return view('admin.pages.brand.form', compact(
            'brand'
        ));
    }

    public function update(BrandRequest $request, $id)
    {
        $this->handleSubmitRequest($request, $id);

        return response()->json([
            'message' => 'Sửa thành công',
            'status' => 'success',
            'url' => route('admin.brand.index')
        ]);
    }

    public function destroy($id)
    {
        $this->brandRepository->destroy($id);

        return response()->json([
            'message' => 'Xóa thành công',
            'status' => 'success'
        ]);
    }

    public function handleSubmitRequest($request, $id = null)
    {
        $attributes = $request->validated();
        $attributes['status'] = (int) $attributes['status'];
        if (!is_null($id)) {
            $this->brandRepository->update($attributes, $request->id);
        } else {
            $this->brandRepository->create($attributes);
        }

    }
}
