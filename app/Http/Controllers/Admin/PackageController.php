<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ImageHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\PackageRequest;
use App\Repositories\Contracts\PackageRepositoryInterface;
use Illuminate\Http\Request;

class PackageController extends Controller
{
    protected $packageRepository;

    public function __construct(PackageRepositoryInterface $packageRepository)
    {
        $this->packageRepository = $packageRepository;
    }


    public function index()
    {
        $packages = $this->packageRepository->paginateList();

        return view('admin.pages.package.index', compact(
            'packages'
        ));
    }

    public function create()
    {
        return view('admin.pages.package.form');
    }

    public function store(PackageRequest $request)
    {
        $this->handleSubmitRequest($request);

        return response()->json([
            'message' => 'Thêm thành công',
            'status' => 'success',
            'url' => route('admin.package.index')
        ]);
    }

    public function edit($id)
    {
        $package = $this->packageRepository->findById($id);
        if (empty($package)) return redirect()->route('admin.package.index');

        return view('admin.pages.package.form', compact(
            'package'
        ));
    }

    public function update(PackageRequest $request, $id)
    {
        $this->handleSubmitRequest($request, $id);

        return response()->json([
            'message' => 'Sửa thành công',
            'status' => 'success',
            'url' => route('admin.package.index')
        ]);
    }

    public function destroy($id)
    {
        $this->packageRepository->destroy($id);

        return response()->json([
            'message' => 'Xóa thành công',
            'status' => 'success'
        ]);
    }

    public function handleSubmitRequest($request, $id = null)
    {
        $attributes = $request->validated();
        $attributes['status'] = (int)$attributes['status'];

        if (!is_null($id)) {
            $this->packageRepository->update($attributes, $id);
        } else {
            $this->packageRepository->create($attributes);
        }

    }
}
