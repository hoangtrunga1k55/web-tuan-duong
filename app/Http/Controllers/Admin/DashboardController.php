<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Contracts\CmsAccountRepositoryInterface;
use App\Repositories\Contracts\MemberRepositoryInterface;
use App\Repositories\Contracts\PostRepositoryInterface;
use App\Repositories\Contracts\NotificationRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    protected $cmsAccountRepository;
    protected $memberRepository;
    protected $newRepository;
    protected $notificationRepository;

    public function __construct(CmsAccountRepositoryInterface $cmsAccountRepository,
                                MemberRepositoryInterface $memberRepository,
                                NotificationRepositoryInterface $notificationRepository,
                                PostRepositoryInterface $newRepository)
    {
        $this->cmsAccountRepository = $cmsAccountRepository;
        $this->memberRepository = $memberRepository;
        $this->newRepository = $newRepository;
        $this->notificationRepository = $notificationRepository;
    }

    public function index()
    {
        $countUser = $this->cmsAccountRepository->all()->count();
        $countCustomer = DB::table('contacts')->get()->count();
        $countNew = DB::table('new')->get()->count();
        $orderBy = array('created_at' => 'asc');
        $nru = $this->memberRepository->all(array('*'), [], [], $orderBy)
            ->groupBy(function ($date) {
                return Carbon::parse($date->created_at)->format('d-m-Y');
            });
        $articles = $this->newRepository->all(array('*'), [], [], $orderBy)
            ->groupBy(function ($date) {
                return Carbon::parse($date->created_at)->format('d-m-Y');
            });
        $notify = $this->notificationRepository->all(array('*'), [], [], $orderBy)
            ->groupBy(function ($date) {
                return Carbon::parse($date->created_at)->format('d-m-Y');
            });
        return view('admin.pages.dashboard', compact(
            'countUser', 'countCustomer', 'countNew', 'nru', 'articles', 'notify'
        ));
    }
}
