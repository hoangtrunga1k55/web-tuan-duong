<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateMember;
use App\Repositories\Contracts\MemberRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    protected $memberRepository;

    public function __construct(MemberRepositoryInterface $memberRepository)
    {
        $this->memberRepository = $memberRepository;
    }


    public function index(Request $request)
    {
        $conditions = [];
        if ($request->email) {
            $conditions['where'][] = ['email', 'like', "%" . $request->email . "%"];
        }
        if ($request->phone_number) {
            $conditions['where'][] = ['phone_number', 'like', "%" . $request->phone_number . "%"];
        }
        if ($request->address) {
            $conditions['where'][] = ['address', 'like', "%" . $request->address . "%"];
        }

        $members = $this->memberRepository->paginateList(10, $conditions);
        return view('admin.pages.member.index', compact(
            'members',
            'request'
        ));
    }

    public function edit($id)
    {
        $member = $this->memberRepository->findById($id);
        if (empty($member)) return redirect()->route('members.index');

        return view('admin.pages.member.form', compact(
            'member'
        ));
    }

    public function update(UpdateMember $request, $id)
    {
        $attributes = $request->validated();
        if(!is_null($request->avatar)){
            $attributes['avatar'] = $request->avatar;
        }else{
            unset($attributes['avatar']);
        }
        if(!is_null($request->password)){
            $attributes['password'] = bcrypt($request->password);
        }else{
            unset($attributes['password']);
        }
        if (isset($attributes['birth_day'])) {
            $attributes['birth_day'] = (int) Carbon::createFromFormat('Y-m-d', $request->birth_day)
                ->timestamp;
        }
        $this->memberRepository->update($attributes, $id);
        return response()->json([
            'message' => 'Sửa thành công',
            'status' => 'success',
            'url' => route('admin.member.index')
        ]);
    }

    public function destroy($id)
    {
        $this->memberRepository->destroy($id);
        return response()->json([
            'message' => 'Xóa thành công',
            'status' => 'success'
        ]);
    }
}
