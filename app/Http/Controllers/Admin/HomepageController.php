<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ImageHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\VendorRequest;
use App\Repositories\Contracts\CategoryRepositoryInterface;
use App\Repositories\Contracts\HomepageRepositoryInterface;
use App\Repositories\Contracts\PostRepositoryInterface;
use App\Repositories\Contracts\VendorRepositoryInterface;
use App\Repositories\Contracts\LanguageRepositoryInterface;
use Illuminate\Http\Request;

class HomepageController extends Controller
{

    protected $homePageRepository;
    protected $categoryRepository;
    protected $postRepository;
    protected $languageRepository;

    public function __construct(HomepageRepositoryInterface $homepageRepository,
                                CategoryRepositoryInterface $categoryRepository,
                                PostRepositoryInterface $postRepository,
                                LanguageRepositoryInterface $languageRepository
    )
    {
        $this->homePageRepository = $homepageRepository;
        $this->categoryRepository = $categoryRepository;
        $this->postRepository = $postRepository;
        $this->languageRepository = $languageRepository;
    }

    public function index(Request $request)
    {
        $categories = $this->categoryRepository->all();
        $posts = $this->postRepository->all();

        $section_about = $this->homePageRepository->findBy('section', "about");
        $section_1 = $this->homePageRepository->findBy('section', "1");
        $section_2 = $this->homePageRepository->findBy('section', "2");
        $section_3 = $this->homePageRepository->findBy('section', "3");
        $section_4 = $this->homePageRepository->findBy('section', "4");
        $section_5 = $this->homePageRepository->findBy('section', "5");
        $section_about_en = $this->homePageRepository->findBy('section', "about_en");
        $languages = $this->languageRepository->all();

        return view('admin.pages.homepage.list', compact(
            'categories',
            'posts',
            'section_about',
            'section_1',
            'section_2',
            'section_3',
            'section_4',
            'section_5',
            'languages',
            'section_about_en'
        ));
    }

    public function handleSubmit(Request $request)
    {
        $attributes = [
            'category_id' => $request->category_id,
            'post_ids' => $request->post_ids,
            'title' => $request->title,
            'lang_type' =>$request->lang_type,
        ];
        $this->homePageRepository->updateOrCreate(['section' => $request->section], $attributes);

        return response()->json([
            'message' => 'Sửa thành công',
            'status' => 'success',
            'url' => route('admin.homepage.index')
        ]);
    }
}
