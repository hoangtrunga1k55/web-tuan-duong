<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ImageHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\TagRequest;
use App\Repositories\Contracts\TagRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TagController extends Controller
{
    protected $tagRepository;

    public function __construct(TagRepositoryInterface $tagRepository)
    {
        $this->tagRepository = $tagRepository;
    }


    public function index()
    {
        $tags = $this->tagRepository->paginateList();

        return view('admin.pages.tag.index', compact(
            'tags'
        ));
    }

    public function create()
    {
        return view('admin.pages.tag.form');
    }

    public function store(TagRequest $request)
    {
        $this->handleSubmitRequest($request);

        return response()->json([
            'message' => 'Thêm thành công',
            'status' => 'success',
            'url' => route('admin.tag.index')
        ]);
    }

    public function edit($id)
    {
        $tag = $this->tagRepository->findById($id);

        if (empty($tag)) return redirect()->route('admin.tag.index');

        return view('admin.pages.tag.form', compact(
            'tag'
        ));
    }

    public function update(TagRequest $request, $id)
    {
        $this->handleSubmitRequest($request, $id);


        return response()->json([
            'message' => 'Sửa thành công',
            'status' => 'success',
            'url' => route('admin.tag.index')
        ]);
    }

    public function destroy($id)
    {
        $this->tagRepository->destroy($id);

        return response()->json([
            'message' => 'Xóa thành công',
            'status' => 'success'
        ]);
    }

    public function handleSubmitRequest($request, $id = null)
    {
        $attributes = $request->validated();
        $attributes['slug'] = Str::slug($attributes['name']);
        $attributes['status'] = (int) $attributes['status'];
        if (!is_null($id)) {
            $this->tagRepository->update($attributes, $request->id);
        } else {
            $this->tagRepository->create($attributes);
        }
    }
}
