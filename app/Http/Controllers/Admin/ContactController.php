<?php

namespace App\Http\Controllers\Admin;
use App\Repositories\Contracts\ContactRepositoryInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    protected $contactRepository;

    public function __construct(ContactRepositoryInterface $contactRepository)
    {
        $this->contactRepository = $contactRepository;
    }

    public function index()
    {
        $contacts = $this->contactRepository->paginateList();
        return view('admin.pages.contact.list',compact('contacts'));
    }

    public function getInfo(Request $request){
        $contact = $this->contactRepository->findById($request->id);

        return response()->json([
           'contact' => $contact
        ]);
    }
}
