<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ImageHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\BannerRequest;
use App\Repositories\Contracts\BannerRepositoryInterface;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    protected $bannerRepository;

    public function __construct(BannerRepositoryInterface $bannerRepository)
    {
        $this->bannerRepository = $bannerRepository;
    }


    public function index()
    {
        $banners = $this->bannerRepository->paginateList();

        return view('admin.pages.banner.index', compact(
            'banners'
        ));
    }

    public function create()
    {
        return view('admin.pages.banner.form');
    }

    public function store(BannerRequest $request)
    {
        $this->handleSubmitRequest($request);


        return response()->json([
            'message' => 'Thêm thành công',
            'status' => 'success',
            'url' => route('admin.banner.index')
        ]);
    }

    public function edit($id)
    {
        $banner = $this->bannerRepository->findById($id);

        if (empty($banner)) return redirect()->route('admin.banner.index');

        return view('admin.pages.banner.form', compact(
            'banner'
        ));
    }

    public function update(BannerRequest $request, $id)
    {
        $this->handleSubmitRequest($request, $id);


        return response()->json([
            'message' => 'Sửa thành công',
            'status' => 'success',
            'url' => route('admin.banner.index')
        ]);
    }

    public function destroy($id)
    {
        $this->bannerRepository->destroy($id);

        return response()->json([
            'message' => 'Xóa thành công',
            'status' => 'success'
        ]);
    }

    public function handleSubmitRequest($request, $id = null)
    {
        $attributes = $request->validated();
        $attributes['status'] = (int) $attributes['status'];

        if (!is_null($id)) {
            $this->bannerRepository->update($attributes, $request->id);
        } else {
            $this->bannerRepository->create($attributes);
        }
    }
}
