<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CategoryRequest;
use App\Repositories\Contracts\CategoryRepositoryInterface;
use App\Repositories\Contracts\LanguageRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\ImageHelper;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;


class CategoryController extends Controller
{
    protected $categoryRepository;
    protected $languageRepository;

    public function __construct(CategoryRepositoryInterface $categoryRepository, LanguageRepositoryInterface $languageRepository)
    {
        $this->categoryRepository = $categoryRepository;
        $this->languageRepository = $languageRepository;
    }

    public function index(Request $request)
    {
        $condition = [];
        if (!is_null($request->status)) {
            $condition['where'][] = ['status', 'like', $request->status];
        }
        if (!is_null($request->title)) {
            $condition['where'][] = ['title', 'like', "%" . $request->title . "%"];
        }
        if (!is_null($request->lang_type)) {
            $condition['where'][] = ['lang_type', 'like', "%" . $request->lang_type . "%"];
        }

        $categories = $this->categoryRepository->paginateList(10, $condition);
        $languages = $this->languageRepository->all(['*'],['where'=>[['status','like','1']]]);
        return view('admin.pages.category.list', compact(
            'categories', 'request','languages'
        ));
    }

    public function create()
    {
        $categories = $this->categoryRepository->all();
        $languages = $this->languageRepository->all();
        return view('admin.pages.category.form', compact('categories', 'languages'));
    }


    public function store(CategoryRequest $request)
    {
        $this->handleSubmitRequest($request);

        return response()->json([
            'message' => 'Tạo thành công',
            'status' => 'success',
            'url' => route('admin.category.index')
        ]);
    }

    public function edit($id)
    {
        $condition = [];
        $condition['whereNotIn']['_id'] = [$id];
        $condition['where'][] = ['parent_id', 'not like', "%" . $id . ",%"];
        $categories = $this->categoryRepository->all(["*"], $condition);
        $category = $this->categoryRepository->findById($id);
        $languages = $this->languageRepository->all();
        return view('admin.pages.category.form', compact(
            'category', 'categories', 'languages'
        ));
    }

    public function update(CategoryRequest $request, $id)
    {
        $this->handleSubmitRequest($request, $id);

        return response()->json([
            'message' => 'Sửa thành công',
            'status' => 'success',
            'url' => route('admin.category.index')
        ]);
    }


    public function destroy($id)
    {
        $this->categoryRepository->destroy($id);

        return response()->json([
            'message' => 'Xóa thành công',
            'status' => 'success'
        ]);
    }


    public function handleSubmitRequest($request, $id = null)
    {
        $attributes = $request->validated();
        $attributes['slug'] = Str::slug($attributes['title']);
        $attributes['status'] = (int)$attributes['status'];

        if (isset($attributes['parent_id'])) {
            $parent = $this->categoryRepository->findById($attributes['parent_id']);

            $parent_id = "";
            if (!is_null($parent->parent_id)) {
                $parent_id .= $parent->parent_id;
            }
            $parent_id .= $parent->_id . ",";

            $attributes['parent_id'] = $parent_id;
        }
        if (!is_null($id)) {
            $this->categoryRepository->update($attributes, $request->id);
        } else {
            $this->categoryRepository->create($attributes);
        }

    }
}
