<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ImageHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\SEORequest;
use App\Repositories\Contracts\SEORepositoryInterface;
use Illuminate\Support\Facades\Route;

class SEOController extends Controller
{
    protected $seoRepository;

    public function __construct(SEORepositoryInterface $seoRepository)
    {
        $this->seoRepository = $seoRepository;
    }


    public function index()
    {
        $seo_info = $this->seoRepository->paginateList();

        return view('admin.pages.seo.index', compact(
            'seo_info'
        ));
    }

    public function create()
    {
        $routes = $this->getSEOableRoute();
        return view('admin.pages.seo.form', compact(
            'routes'
        ));
    }

    public function store(SEORequest $request)
    {
        $this->handleSubmitRequest($request);


        return response()->json([
            'message' => 'Thêm thành công',
            'status' => 'success',
            'url' => route('admin.seo.index')
        ]);
    }

    public function edit($id)
    {
        $info = $this->seoRepository->findById($id);
        $routes = $this->getSEOableRoute();
        if (empty($info)) return redirect()->route('admin.seo.index');
        return view('admin.pages.seo.form', compact(
            'info',
            'routes'
        ));
    }

    public function update(SEORequest $request, $id)
    {
        $this->handleSubmitRequest($request, $id);

        return response()->json([
            'message' => 'Sửa thành công',
            'status' => 'success',
            'url' => route('admin.seo.index')
        ]);
    }

    public function destroy($id)
    {
        $this->seoRepository->destroy($id);

        return response()->json([
            'message' => 'Xóa thành công',
            'status' => 'success'
        ]);
    }

    public function handleSubmitRequest($request, $id = null)
    {
        $attributes = $request->validated();
        if (!is_null($id)) {
            $this->seoRepository->update($attributes, $id);
        } else {
            $this->seoRepository->create($attributes);
        }

    }

    public function getSEOableRoute(){
        $routeCollection = Route::getRoutes();
        $arr = [];
        foreach ($routeCollection as $route) {
            if (in_array('SEOable', $route->action['middleware'] ?? [])) {
                $arr[] = [
                    'name' => $route->action['as'],
                    'uri' =>$route->uri
                ];
            }
        }
        return $arr;

    }
}
