<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ConfigRequest;
use App\Repositories\Contracts\ConfigRepositoryInterface;
use App\Repositories\Contracts\LanguageRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ConfigController extends Controller
{
    protected $configRepository;
    protected $languageRepository;

    public function __construct(ConfigRepositoryInterface $configRepository,LanguageRepositoryInterface $languageRepository)
    {
        $this->configRepository = $configRepository;
        $this->languageRepository = $languageRepository;
    }


    public function index(Request $request)
    {
        $conditions = [];
        if (!is_null($request->code)) {
            $conditions['where'][] = ['lang_type', 'like', '%' . $request->code . '%'];
        }
        if (!is_null($request->key)) {
            $conditions['where'][] = ['key', 'like', '%' . $request->key . '%'];
        }
        if (!is_null($request->lang_type)) {
            $conditions['where'][] = ['lang_type', 'like', "%" . $request->lang_type . "%"];
        }
        $configs = $this->configRepository->paginateList(10, $conditions);

        $languages = $this->languageRepository->all(['*'],['where'=>[['status','like','1']]]);
        return view('admin.pages.config.list', compact('configs', 'request','languages'));
    }

    public function create()
    {
        $languages = $this->languageRepository->all();
        return view('admin.pages.config.form',compact('languages'));
    }

    public function store(ConfigRequest $request)
    {
        $this->handleSubmitRequest($request);


        return response()->json([
            'message' => 'Thêm thành công',
            'status' => 'success',
            'url' => route('admin.config.index')
        ]);
    }

    public function edit($id)
    {
        $config = $this->configRepository->findById($id);
        $languages = $this->languageRepository->all();
        return view('admin.pages.config.form', compact('config','languages'));
    }

    public function update(ConfigRequest $request, $id)
    {

        $this->handleSubmitRequest($request, $id);

        return response()->json([
            'message' => 'Sửa thành công',
            'status' => 'success',
            'url' => route('admin.config.index')
        ]);
    }

    public function destroy($id)
    {
        $this->configRepository->destroy($id);

        return response()->json([
            'message' => 'Xóa thành công',
            'status' => 'success'
        ]);
    }

    public function handleSubmitRequest($request, $id = null)
    {
        $attributes = $request->validated();
        $attributes['status'] = (int)$attributes['status'];

        if (!is_null($id)) {
            $this->configRepository->update($attributes, $id);
        } else {
            $this->configRepository->create($attributes);
        }

    }
}
