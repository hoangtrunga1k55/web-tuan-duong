<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\PushNotifyHelper;
use App\Http\Requests\NotifyRequest;
use App\Repositories\Contracts\CmsAccountRepositoryInterface;
use App\Repositories\Contracts\MemberRepositoryInterface;
use App\Repositories\Contracts\NotificationRepositoryInterface;
use Aws\Rds\AuthTokenGenerator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    protected $notificationRepository;
    protected $cmsAccountRepository;
    protected $memberRepository;

    public function __construct(
        NotificationRepositoryInterface $notificationRepository,
        CmsAccountRepositoryInterface $cmsAccountRepository,
        MemberRepositoryInterface $memberRepository
    )
    {
        $this->notificationRepository = $notificationRepository;
        $this->cmsAccountRepository = $cmsAccountRepository;
        $this->memberRepository = $memberRepository;
    }


    public function index(Request $request)
    {
        $conditions = [];
        if (!is_null($request->title)) {
            $conditions['where'][] = ['title', 'like', '%' . $request->title . '%'];
        }
        if (!is_null($request->key)) {
            $conditions['where'][] = ['key', 'like', '%' . $request->key . '%'];
        }
        $notifications = $this->notificationRepository->paginateList(10, $conditions);
        return view('admin.pages.notification.list', compact('notifications', 'request'));
    }

    public function create()
    {
        $users = $this->memberRepository->all();
        return view('admin.pages.notification.form', compact('users'));
    }

    public function store(NotifyRequest $request)
    {
        $this->handleSubmitRequest($request);

        return response()->json([
            'message' => 'Thêm mới thành công',
            'status' => 'success',
            'url' => route('admin.notification.index')
        ]);
    }


    public function destroy($id)
    {
        $this->notificationRepository->destroy($id);

        return response()->json([
            'message' => 'Xóa thành công',
            'status' => 'success'
        ]);
    }

    public function handleSubmitRequest($request)
    {
        $attributes = [];
        $attributes['title'] = $request->title;
        $attributes['body'] = $request->body;
        $attributes['type'] = $request->type;

        $attributes['data'] = [
            'type' => "topic",
        ];
        if ($attributes['type'] === "topic") {
            $attributes['topic'] = $request->topic;
            $result = PushNotifyHelper::pushToTopic(
                $attributes['topic'],
                $attributes['title'],
                $attributes['body'],
                $attributes['data']
            );
            $attributes['status'] = $result->isSuccess();
            $attributes['sender'] = Auth::id();
            $this->notificationRepository->create($attributes);
        } else {
            foreach ($request->user_ids as $user_id) {
                $attributes['user_id'] = $user_id;
                $result = PushNotifyHelper::pushToTopic(
                    $attributes['user_id'],
                    $attributes['title'],
                    $attributes['body'],
                    $attributes['data']
                );
                $attributes['status'] = $result->isSuccess();
                $attributes['sender'] = Auth::id();
                $this->notificationRepository->create($attributes);
            }
        }
    }
}
