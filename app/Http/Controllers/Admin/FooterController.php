<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\HeaderRequest;
use App\Repositories\Contracts\FooterRepositoryInterface;
use App\Repositories\Contracts\LanguageRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class FooterController extends Controller
{
    protected $languageRepository;

    public function __construct(FooterRepositoryInterface $headerRepository, LanguageRepositoryInterface $languageRepository)
    {
        $this->headerRepository = $headerRepository;
        $this->languageRepository = $languageRepository;
    }


    public function index()
    {
        $footers = $this->headerRepository->paginateList(10);
        return view('admin.pages.footer.index', compact(
            'footers'
        ));
    }

    public function create()
    {
        $languages = $this->languageRepository->all();
        return view('admin.pages.footer.form', compact('languages'));
    }

    public function store(HeaderRequest $request)
    {
        $this->handleSubmitRequest($request);


        return response()->json([
            'message' => 'Thêm thành công',
            'status' => 'success',
            'url' => route('admin.footer.index')
        ]);
    }

    public function edit($id)
    {
        $languages = $this->languageRepository->all();
        $footer = $this->headerRepository->findById($id);
        if (empty($footer)) return redirect()->route('admin.footer.index');

        return view('admin.pages.footer.form', compact(
            'footer', 'languages'
        ));
    }

    public function update(HeaderRequest $request, $id)
    {
        $this->handleSubmitRequest($request, $id);


        return response()->json([
            'message' => 'Sửa thành công',
            'status' => 'success',
            'url' => route('admin.footer.index')
        ]);
    }

    public function destroy($id)
    {
        $this->headerRepository->destroy($id);

        return response()->json([
            'message' => 'Xóa thành công',
            'status' => 'success'
        ]);
    }

    public function handleSubmitRequest($request, $id = null)
    {
        $attributes = $request->validated();
        $attributes['status'] = (int)$attributes['status'];

        if (!is_null($id)) {
            $this->headerRepository->update($attributes, $id);
        } else {
            $this->headerRepository->create($attributes);
        }

    }
}
