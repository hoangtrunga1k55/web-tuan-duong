<?php

namespace App\Http\Controllers\Site;

use App\Helpers\DefaultLanguage;
use App\Models\Post;
use App\Repositories\Contracts\CategoryRepositoryInterface;
use App\Repositories\Contracts\ConfigRepositoryInterface;
use App\Repositories\Contracts\FooterRepositoryInterface;
use App\Repositories\Contracts\LanguageRepositoryInterface;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class SiteController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected $lang;
    public function getDefaultLanguage(){
        if (!Cache::get('languages')){
            Cache::forever('languages','vn');
        }
        return Cache::get('languages');
    }
    public function __construct()
    {
        parent::__construct();
        $lasted_posts = DB::table('posts')
            ->where('published_at', '<=', now())
            ->where('status', 'like', Post::RELEASE)
            ->whereNull('deleted_at')
            ->get();
        $condition_child_category_news['where'][] = ['show', 'like', 1];
        if ($this->getDefaultLanguage()=='vn'){
            $condition_child_category_news['where'][] = ['parent_id', 'like', "60822e0b4e0fa31ba8426528,"];
            $child_category_news = app(CategoryRepositoryInterface::class)->all(array('*'), $condition_child_category_news);
        } else{
            $condition_child_category_news['where'][] = ['parent_id', 'like', "60828157373c4d3df81a638a,"];
            $child_category_news = app(CategoryRepositoryInterface::class)->all(array('*'), $condition_child_category_news);
        }

        $feature_posts = DB::table('posts')
            ->where('published_at', '<=', now())
            ->where('feature_post', 'like', 1)
            ->where('status', 'like', Post::RELEASE)
            ->where('lang_type','like',$this->getDefaultLanguage())
            ->whereNull('deleted_at')
            ->limit(5)
            ->get();
        $config_conditions['where'][] = ['status', 'like', 1];
        $config_conditions['whereIn']['lang_type'] = [$this->getDefaultLanguage(),'all'];
        $all_configs = app(ConfigRepositoryInterface::class)->all(array('*'),$config_conditions);
        $configs = [];
        foreach ($all_configs as $config) {
            $configs[$config['key']] = $config['content'];
        }
        $brands = DB::table('brands')->where('status', 'like', 1)->get();


        $condition = [];
        $condition['where'][] = ['show', 'like', 1];
        $condition['whereNull'][] = ['parent_id'];
//        $condition['where'][] = ['lang_type', 'like', Config::get('app.locale')];
        $order = ['priority' => 'asc'];

        $category_headers = app(CategoryRepositoryInterface::class)->all(array('*'), $condition, [], $order);
        $footer_condition = [];
        $footer_condition['where'][] = ['status', 'like', 1];
        $footer = app(FooterRepositoryInterface::class)->all(array('*'));
        $languages = app(LanguageRepositoryInterface::class)->all(array('*'), $footer_condition);
        View::share('languages', $languages);
        View::share('configs', $configs);
        View::share('category_headers', $category_headers);
        View::share('footer', $footer);
        View::share('all_configs', $all_configs);
        View::share('brands', $brands);
        View::share('child_category_news', $child_category_news);
        View::share('lasted_posts', $lasted_posts);
        View::share('feature_posts', $feature_posts);
    }

    public function sitemap()
    {
        // create new sitemap object
        $sitemap = \App::make('sitemap');

        $sitemap->setCache('laravel.sitemap', 60);

        if (!$sitemap->isCached()) {
            $sitemap->add(URL::to('/'), (string)now(), '1.0', 'daily');
            $sitemap->add(URL::to('tin-tuc'), (string)now(), '1.0', 'daily');
            // add item with translations (url, date, priority, freq, images, title, translations)
            $translations = [
//            ['language' => 'fr', 'url' => URL::to('pageFr')],
//            ['language' => 'de', 'url' => URL::to('pageDe')],
//            ['language' => 'bg', 'url' => URL::to('pageBg')],
            ];
//        $sitemap->add(URL::to('pageEn'), '2015-06-24T14:30:00+02:00', '0.9', 'monthly', [], null, $translations);

            // add item with images
            $images = [
//            ['url' => URL::to('images/pic1.jpg'), 'title' => 'Image title', 'caption' => 'Image caption', 'geo_location' => 'Plovdiv, Bulgaria'],
//            ['url' => URL::to('images/pic2.jpg'), 'title' => 'Image title2', 'caption' => 'Image caption2'],
//            ['url' => URL::to('images/pic3.jpg'), 'title' => 'Image title3'],
            ];
//        $sitemap->add(URL::to('post-with-images'), '2015-06-24T14:30:00+02:00', '0.9', 'monthly', $images);

            // get all news from db
            $news_list = DB::table('new')
                ->where('status', "like", News::RELEASE)
                ->whereNull('deleted_at')
                ->orderBy('created_at', 'desc')
                ->get();

            foreach ($news_list as $news) {
                $sitemap->add(URL::to('bai-viet/' . $news['slug']), (string)now(), "1.0", "daily");
            }
            $categories = DB::table('category')
                ->where('status', "like", "1")
                ->whereNull('deleted_at')
                ->orderBy('created_at', 'desc')
                ->get();

            // add every category to the sitemap
            foreach ($categories as $category) {
                $sitemap->add(URL::to('danh-muc/' . $category['slug']), (string)now(), "1.0", "daily");
            }
            // get all tag from db
//            $tags = DB::table('tags')
//                ->where('status', "like", 1)
//                ->orderBy('created_at', 'desc')
//                ->whereNull('deleted_at')
//                ->get();
//            // add every tag to the sitemap
//            foreach ($tags as $tag) {
//                $sitemap->add(URL::to('tag/'. $tag['slug']), (string) now(), "1.0", "daily");
//            }
        }
        return $sitemap->render('xml');
    }
}
