<?php

namespace App\Http\Controllers\Site;

use App\Helpers\DefaultLanguage;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use App\Repositories\Contracts\CategoryRepositoryInterface;
use App\Repositories\Contracts\FooterRepositoryInterface;
use App\Repositories\Contracts\HeaderRepositoryInterface;
use App\Repositories\Contracts\HomepageRepositoryInterface;
use App\Repositories\Contracts\LanguageRepositoryInterface;
use App\Repositories\Contracts\PostRepositoryInterface;
use App\Repositories\Contracts\PackageRepositoryInterface;
use App\Repositories\Contracts\TagRepositoryInterface;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class BlogController extends SiteController
{
    protected $postRepository;
    protected $categoryRepository;
    protected $tagRepository;
    protected $packageRepository;
    protected $headerRepository;
    protected $languageRepository;
    protected $footerRepository;
    protected $homePageRepository;

    public function __construct(
        HomepageRepositoryInterface $homepageRepository,
        PostRepositoryInterface $postRepository,
        CategoryRepositoryInterface $categoryRepository,
        PackageRepositoryInterface $packageRepository,
        TagRepositoryInterface $tagRepository,
        HeaderRepositoryInterface $headerRepository,
        FooterRepositoryInterface $footerRepository,
        LanguageRepositoryInterface $languageRepository
    )
    {
        parent::__construct();
        $this->homePageRepository = $homepageRepository;
        $this->postRepository = $postRepository;
        $this->categoryRepository = $categoryRepository;
        $this->packageRepository = $packageRepository;
        $this->tagRepository = $tagRepository;
        $this->headerRepository = $headerRepository;
        $this->footerRepository = $footerRepository;
        $this->languageRepository = $languageRepository;
    }


    public function index()
    {
        $condition['where'][] = ['section','=','about'];
        $condition['where'][] = ['lang_type','=',Config::get('app.locale')];
        if (Config::get('app.locale') == 'vn'){
            $section_about = $this->homePageRepository->findBy('section', "about");
            $section_1 = $this->homePageRepository->findBy('section', "1");
            $section_2 = $this->homePageRepository->findBy('section', "2");
            $section_3 = $this->homePageRepository->findBy('section', "3");
            $section_4 = $this->homePageRepository->findBy('section', "4");
            $section_5 = $this->homePageRepository->findBy('section', "5");
        }
        else{
            $section_about = $this->homePageRepository->findBy('section', "about_en");
            $section_1 = $this->homePageRepository->findBy('section', "1_en");
            $section_2 = $this->homePageRepository->findBy('section', "2_en");
            $section_3 = $this->homePageRepository->findBy('section', "3_en");
            $section_4 = $this->homePageRepository->findBy('section', "4_en");
            $section_5 = $this->homePageRepository->findBy('section', "5");
        }
        return view('site.pages.index.master', compact(
            'section_about',
            'section_1',
            'section_2',
            'section_3',
            'section_4',
            'section_5'

        ));
    }

    public function getPostsByCategory($slug)
    {
        $category_conditions = [];
        $category_conditions['where'][] = ['slug', 'like', $slug];
        $category_conditions['where'][] = ['status', 'like', 1];
        $category = $this->categoryRepository->findOneByConditions($category_conditions);

        $child_categories_conditions = [];
        $child_categories_conditions['where'][] = ['parent_id', 'like', $category->id . ',%'];
        $child_categories_conditions['where'][] = ['status', 'like', 1];
        $child_categories_conditions['where'][] = ['show', 'like', 1];
        $child_categories_conditions['where'][] = ['lang_type', 'like', Config::get('app.locale')];
        $child_categories = $this->categoryRepository->all(array('*'), $child_categories_conditions);
        $posts = $category->news
            ->where('published_at', '<=', now())
            ->where('status', 'like', Post::RELEASE)
            ->where('lang_type', 'like', Config::get('app.locale'))
            ->sortBy('priority');
        return view('site.pages.posts.master', compact(
            'category',
            'child_categories',
            'posts',
            'slug'
        ));
    }

    public function getDetailPost($post_slug)
    {

        $post_conditions = [];
//        $post_conditions['where'][] = ['lang_type', 'like', Config::get('app.locale')];
//        $post_conditions['where'][] = ['category_id', 'like', '%' . $category->id . ',%'];
        $post_conditions['where'][] = ['slug', 'like', $post_slug];
        $post = $this->postRepository->findBy('slug', $post_slug);
        if(is_null( $post)){
            return redirect()->route('site.index');
        }
        return view('site.pages.post.master', compact('post'));

    }

    public function changeLanguage($language)
    {
        \Illuminate\Support\Facades\Session::put('language', $language);
        return redirect()->back();
    }
}
