<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContactRequest;
use App\Repositories\Contracts\ContactRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ContactController extends SiteController
{
    protected $contactRepository;

    public function __construct(ContactRepositoryInterface $contactRepository)
    {
        parent::__construct();
        $this->contactRepository = $contactRepository;
    }

    public function show(){
        return view('site.pages.contact.master');
    }

    public function store(ContactRequest $request)
    {
        $attributes = $request->validated();

        $this->contactRepository->create($attributes);

        return response()->json([
            'status' => 'success',
            'message' => 'Chúng tôi đã tiếp nhận phản hồi của bạn'
        ]);

        $bot_token = "1784190800:AAEfqs0gs0QDI1rj-uQPt6Oa5b7NJXqk3To";
        $chat_id = "-1001180387496";

        $content = urlencode(
            "Họ tên: {$request->name}\n" .
            "Số điện thoại: {$request->phone}\n" .
            "Email: {$request->email}\n" .
            "Lĩnh vực: {$request->business_type}\n" .
            "Nội dung: \n".
            "{$request->message}"
        );
        Http::get("https://api.telegram.org/bot{$bot_token}/sendMessage?text={$content}&chat_id={$chat_id}");

    }
}
