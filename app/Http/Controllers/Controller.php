<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class Controller extends BaseController
{
    protected $lang;
    public function changeLanguage($language)
    {
//      \Illuminate\Support\Facades\Session::put('language', $language);
      Cache::forever('languages',$language);
        return redirect()->route('site.index');
    }

    public function __construct()
    {
        $this->lang = Config::get('app.locale');
    }

    public function getLanguage(){
        return $this->lang;
    }

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
