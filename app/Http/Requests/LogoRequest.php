<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LogoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => '',
            'link' => '',
            'status' => 'required|in:0,1',
            'image' => '',
        ];

        if (!$this->id) {
            $rules['image'] .= '|required';
            return $rules;
        }
        return $rules;
    }
}
