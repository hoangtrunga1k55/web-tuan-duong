<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ConfigRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required',
            'key' => 'required',
            'status' => 'required|in:0,1',
            'type' => 'required|in:0,1',
            'content' => 'required',
            'lang_type'=>'required'
        ];
        return $rules;
    }
    public function messages()
    {
        return [
            'title.required' => 'Vui lòng nhâp tiêu đề',
            'title.max' => 'Tiêu đề phải nhỏ hơn 255 ký tự',
            'key.required' => 'Vui lòng nhâp key',
            'key.max' => 'Key phải nhỏ hơn 255 ký tự',
            'content.required' => 'Vui lòng nhập nội dung',
            'status.in' => 'Vui lòng nhập đúng định dạng 0,1',
        ];
    }

}
