<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required|max:70',
            'description' => 'required|max:160',
            'meta_title' => 'max:70',
            'meta_description' => 'max:160',
            'meta_keywords' => '',
            'meta_thumbnail' => '',
            'status' => 'required|in:0,1',
            'image' => '',
            'parent_id' => '',
            'lang_type'=>'required',
            'type' => '',
            'show' => '',
            'priority'=>''
        ];
        if(!is_null($this->id)){
            return $rules;
        }
//        $rules['image'] .= 'required';
//        $rules['meta_thumbnail'] .= 'required';
        return $rules;
    }
    public function messages()
    {
        return [
            'title.required' => 'Vui lòng nhâp tiêu đề',
            'slug.required' => 'Vui lòng nhâp đường dẫn rút gọn',
            'title.max' => 'Tiêu đề phải nhỏ hơn 255 ký tự',
            'description.required' => 'Vui lòng nhập mô tả',
            'status.in' => 'Vui lòng nhập đúng định dạng 0,1',
            'image.required' => 'Vui lòng nhập hình ảnh',
            'image.image'=>'Chỉ chấp nhận file ảnh'
        ];
    }

}
