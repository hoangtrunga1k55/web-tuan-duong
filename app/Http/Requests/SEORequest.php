<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SEORequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required|max:70',
            'description' => 'required|max:160',
            'keywords' => 'required',
            'image' => '',
            'route' => 'required|unique:seo,route',
            'site_name' => 'required',
        ];
        if (!is_null($this->id)) {
            $rules['route'] .= ",{$this->id},_id";
        }
        return $rules;
    }
    public function messages()
    {
        return [
            'title.required' => 'Vui lòng nhâp tiêu đề',
            'title.max' => 'Tiêu đề phải nhỏ hơn 70 ký tự',
            'description.required' => 'Vui lòng nhập mô tả',
        ];
    }

}
