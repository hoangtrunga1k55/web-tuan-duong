<?php

namespace App\Http\Middleware;

use App\Helpers\PermissionsHelper;
use App\Repositories\Contracts\SEORepositoryInterface;
use Closure;
use Illuminate\Support\Facades\Auth;

class SEOable
{

    protected $seoRepository;

    public function __construct(SEORepositoryInterface $seoRepository)
    {
        $this->seoRepository = $seoRepository;
    }

    public function handle($request, Closure $next)
    {
        $meta = $this->seoRepository->findBy('route',$request->route()->getName());
        $request->merge(array("meta" => $meta));
        return $next($request);
    }
}
