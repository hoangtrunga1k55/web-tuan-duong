<?php

namespace App\Http\Middleware;

use App\Helpers\DefaultLanguage;
use Closure;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class LanguageMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $lang = Cache::get('languages');
        config(['app.locale' => $lang]);
        return $next($request);
    }
}
