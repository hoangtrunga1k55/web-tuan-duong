<?php

namespace App\Providers;

use Illuminate\Support\Env;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
         if (\env('APP_SSL', false)) {
             URL::forceScheme('https');
         }

        $models = [
            "CmsAccount",
            "Category",
            "Post",
            "PasswordReset",
            "Log",
            "Config",
            "Banner",
            "Contact",
            "Role",
            "Permission",
            "Contact",
            "Package",
            "Brand",
            "Logo",
            "Tag",
            "Header",
            "Footer",
            "Vendor",
            "SEO",
            "Notification",
            "Member",
            "Language",
            "Homepage",
            "Product",
        ];

        foreach ($models as $model) {
            $this->app->bind('App\Repositories\Contracts\\' . $model . 'RepositoryInterface', 'App\Repositories\Eloquents\\' . $model . 'Repository');
        }
    }
}
