<?php


namespace App\Helpers;


use App\Repositories\Contracts\CategoryRepositoryInterface;
use App\Repositories\Contracts\LogRepositoryInterface;
use Illuminate\Support\Facades\Auth;

class  LogsHelper
{
    protected static $desciption="";
    protected static $params="";
    public static function getDescription($title,$id,$param){
        self::$desciption= isset($title)?$title.$id.$param:"";
        return self::$desciption;
    }
    public static function getParams($params_data,$request){
        foreach ($params_data as $param){
            self::$params.= empty($request[$param])?"":" ".$param.":".$request[$param];
        }
        return  self::$params;
    }
    public static function  save($request,$route)
    {
        $data = [
            'email' => Auth::user()->email,
            'request_data'=>json_encode($request),
            'ip'=> $_SERVER['REMOTE_ADDR'],
            'browser' =>$_SERVER['HTTP_USER_AGENT']
        ];
        //add title
        $extra_title = $route['title'];
        $title = empty($extra_title)?"":$extra_title;
        // add id
        $insert_id = $route['insert_id'];
        $id = empty($request[$insert_id])?"":" ".$request[$insert_id];
        //add extra_params[]
        $extra_name_params = $route['extra_name_params'];
        $params =empty($extra_name_params)?"":self::getParams($extra_name_params,$request);
        //get Description
        $description = self::getDescription($title,$id,$params);
        $data['description'] = $description;
        return $data;
    }
}
