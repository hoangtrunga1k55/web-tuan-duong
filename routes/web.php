<?php

use App\Http\Controllers\Site\ContactController;
use App\Http\Controllers\Site\SiteController;
use App\Models\Post;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\Site\BlogController;
use \App\Http\Controllers\Admin\CategoryController;
use \App\Http\Controllers\Admin\PostController;
use \App\Http\Controllers\Admin\LanguageController;
use Illuminate\Support\Facades\URL;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group([
    'middleware' => ['SEOable','locale','auth_logs']
], function (){
    Route::get('/', [BlogController::class, 'index'])->name('site.index');
    Route::get('/danh-muc/{slug}', [BlogController::class, 'getPostsByCategory'])->name('site.category');

    Route::get('/bai-viet/{slug}', [BlogController::class, 'getDetailPost'])->name('site.post.detail');
    Route::get('/lien-he/', [ContactController::class, 'show'])->name('site.contact');
    Route::get('/contact/', [ContactController::class, 'show'])->name('site.contact_en');
    Route::post('/gui-thong-tin/', [ContactController::class, 'store'])->name('site.submit-contact');
});

//Route::get('/tag/{slug}', [BlogController::class, 'getNewsByTag'])->name('tag.list');
Route::get('sitemap.xml', [SiteController::class, 'sitemap']);

Route::get('/language/{language}',[\App\Http\Controllers\Controller::class, 'changeLanguage'])->name('languageWeb');
