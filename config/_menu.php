<?php

return [
    [
        'title' => 'Dashboard',
        'route' => 'cp-admin',
        'permission' => '/',
        'check_active_prefix' => '',
        'icon_class' => 'fa fa-user',
        'sub_menus' => []
    ],
    [
        'title' => 'Category',
        'permission' => '',
        'route' => "admin.category.index",
        'check_active_prefix' => 'cp-admin',
        'icon_class' => 'fa fa-edit',
        'sub_menus' =>
            [
                [
                    'title' => 'List Category',
                    'permission' => '',
                    'route' => "admin.category.index",
                    'check_active_prefix' => 'cp-admin',
                    'icon_class' => 'fa fa-edit',
                    'sub_menus' => []
                ],
                [
                    'title' => 'Add Category',
                    'permission' => '',
                    'route' => "admin.category.create",
                    'check_active_prefix' => 'cp-admin',
                    'icon_class' => 'fa fa-edit',
                    'sub_menus' => [],
                ]
            ]
    ],
    [
        'title' => 'News',
        'permission' => '',
        'route' => "admin.new.index",
        'check_active_prefix' => 'cp-admin',
        'icon_class' => 'fa fa-edit',
        'sub_menus' => [
            [
                'title' => 'List New',
                'permission' => '',
                'route' => "admin.new.index",
                'check_active_prefix' => 'cp-admin',
                'icon_class' => 'fa fa-edit',
                'sub_menus' => []
            ],
            [
                'title' => 'Add New',
                'permission' => '',
                'route' => "admin.new.create",
                'check_active_prefix' => 'cp-admin',
                'icon_class' => 'fa fa-edit',
                'sub_menus' => []
            ]
        ]
    ]
];

