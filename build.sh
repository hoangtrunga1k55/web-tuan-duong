#!/usr/bin/env bash
#!/bin/bash
IMAGE="registry.gitlab.com/hap-tech/web-tuan-duong"

echo "#######   Processing  ###########"
echo "#######   Building on $IMAGE ###########"
docker build -t $IMAGE .
echo "#######   Pushing on $IMAGE ###########"
docker push $IMAGE

echo "###############################################################"
