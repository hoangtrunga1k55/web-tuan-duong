<div class="dhc">
    <div class="divm">
        @include('site.pages.index.section.section_about', ['section' => $section_about, 'post' => !is_null($section_about) ? $section_about->posts()->first() : []])
        @include('site.pages.index.section.section_1', ['posts' => !is_null($section_1) ? $section_1->posts() : []])
    </div>
    <div class="divr">

        @include('site.layouts.lasted_posts')
    </div>
    <div class="cl"></div>
</div>
@include("site.pages.index._projects")
@include("site.pages.index._info")
