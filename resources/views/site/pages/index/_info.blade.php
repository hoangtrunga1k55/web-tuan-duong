<div class="g1">&nbsp;</div>
<div class="ds2">
    @include('site.pages.index.section.maps_api', ['section' => $section_5, 'posts' => !is_null($section_5) ? $section_5->posts() : []])
    <div class="cl"></div>
    <script type="text/javascript" src="{{ asset("assets/site/js/imgsliw/jquery_1.js") }}"></script>
</div>
<ul class="mnf">
    @include('site.pages.index.section.section_3+4', ['section' => $section_3, 'posts' => !is_null($section_3) ? $section_3->posts() : []])

    @include('site.pages.index.section.section_3+4', ['section' => $section_4, 'posts' => !is_null($section_4) ? $section_4->posts() : []])
</ul>
<div class="cl"></div>
