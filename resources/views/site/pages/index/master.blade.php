@extends('site.layouts.index')
@section('seo')
    <title>{{ $configs['companyName'] ?? "companyName" }}</title>
    <meta name="keywords" content="{{ $configs['companyName'] ?? "companyName" }}"/>
    <meta name="description" content="{{ $configs['companyName'] ?? "companyName" }}"/>
@endsection

@section('content')
    @include("site.layouts.feature_news")
    @include("site.pages.index._main_content")
@endsection
@section('javascript')
    <script>
    </script>
@endsection

