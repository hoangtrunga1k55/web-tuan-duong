
<div class="t">{{ $section->title }}</div>
<div class="l"><a href="javascript:void(0);" id="slide-adv-back"></a></div>
<div class="skinimg">
    <ul id="ul-box-adv">
        @foreach($posts as $post)
        <li>
            <a href="{{ route('site.post.detail', ['slug' => $post['slug']]) }}"><img
                    src="{{ $post['image'] ?? '' }}"
                    alt="{{ $post['title'] ?? '' }}"/></a>
            <strong>
                <a href="{{ route('site.post.detail', ['slug' => $post['slug']]) }}">
                    {{ $post['title'] ?? '' }}
                </a>
            </strong>
            <span>{{ $post['description'] ?? '' }}</span>
        </li>
        @endforeach
    </ul>
</div>
<div class="r"><a href="javascript:void(0);" id="slide-adv-next"></a></div>
