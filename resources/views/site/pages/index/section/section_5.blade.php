<div class="t"><h3>{{ $section->title ?? "" }}</h3></div>
<div class="l"><a href="javascript:void(0);" id="back1"></a></div>
<div class="skinimg">
    <ul id="adv1">
        @foreach($posts as $post)
            <li>
                <strong>
                    <a href="{{ route('site.post.detail', ['slug' => $post['slug']]) }}">
                        {{ $post['title'] ?? "" }}
                    </a>
                </strong>
                <span>&nbsp;</span>
                <a href="{{ route('site.post.detail', ['slug' => $post['slug']]) }}">
                    <img class="m" src="{{ $post['image'] ?? "" }}" alt="{{ $post['title'] ?? "" }}"/>
                </a>
            </li>
        @endforeach

    </ul>
</div>
<div class="r"><a href="javascript:void(0);" id="next1"></a></div>
