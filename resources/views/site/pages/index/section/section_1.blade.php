<div class="cl"></div>
<br>
<ul class="umain">
    <div class="skinimg">
        <ul id="ul-box-adv-2">
            @foreach($posts as $post)
                <li class="c">
                    <strong>
                        <a href="{{ route('site.post.detail', ['slug' => $post['slug']]) }}"
                           title="{{ $post['title'] ?? "" }}">
                            {{ $post['title'] ?? "" }}</a></strong>
                    <a href="{{ route('site.post.detail', ['slug' => $post['slug']]) }}">
                        <img src="{{ $post['image'] ?? "" }} "
                             alt="{{ $post['title'] ?? "" }}"/></a>
                    <span>
                        {{ $post['description'] ?? "" }}
                    </span>
                    <a class="f" href="{{ route('site.post.detail', ['slug' => $post['slug']]) }}"
                       rel="nofollow">{!! __('show_more') !!}</a>
                </li>
            @endforeach
        </ul>
    </div>
</ul>
