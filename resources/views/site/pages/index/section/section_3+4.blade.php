<li>
    <strong>
        <a href="{{ isset($section->category) ? route('site.category', ['slug' => $section->category->slug]) : "#" }}" title="{{ $section->title ?? "" }}">
            {{ $section->title ?? "" }}
        </a>
    </strong>
    @foreach($posts as $post)
    <span>
        <a href="{{ route('site.post.detail', ['slug' => $post['slug']]) }}">
            {{ $post['title'] ?? "" }}
        </a>
    </span>
    @endforeach
</li>
