<div class="dhc">
    <div class="dpro">
        <h1 class="h1p">{{ $post->title }}</h1>
        <div class="intro">
            {!! $post->content !!}
            <div class="cl"></div>
        </div>
        {{--        <div class="dkeyn">--}}
        {{--            <a href="/tags/hcci-to-chuc-thanh-cong-dai-hoi-dong-co-dong-thuong-nien-2020-1.html">--}}
        {{--                HCCI tổ chức thành công Đại hội đồng cổ đông thường niên 2020--}}
        {{--            </a>--}}
        {{--        </div>--}}

        <div
            style="color:#003399;font-weight:bold;font-size:13px;border-bottom:solid 1px #9dc900;height:25px; padding:0px 5px;">
            <div style="float:right;">
                <a rel="nofollow"
                   href="http://www.facebook.com/share.php?u={{ route('site.post.detail', $post->slug) }}&title={{ $post->title }}"
                   target="socialbookmark">
                    <img title="Submit Thread to Facebook" src="{{ asset('assets/site/images/icon/facebook.gif') }}"
                         alt="Submit Thread to Facebook">
                </a>
                <a rel="nofollow"
                   href="http://twitter.com/home?status={{ $post->title }} - {{ route('site.post.detail', $post->slug) }}"
                   target="socialbookmark"><img title="Submit Thread to Twitter" src="{{ asset('assets/site/images/icon/twitter.png') }}"
                                                alt="Submit Thread to Twitter"></a>
                <a rel="nofollow"
                   href="http://www.google.com/bookmarks/mark?op=edit&output=popup&bkmk={{ route('site.post.detail', $post->slug) }}&title={{ $post->title }}"
                   target="socialbookmark"><img title="Submit Thread to Google" src="{{ asset('assets/site/images/icon/google.gif') }}"
                                                alt="Submit Thread to Google"></a>
            </div>
            <img src=" {{asset('assets/site//images/icon/awaiting.gif') }}" style="vertical-align:middle;" alt="icon"> Các tin tức khác
        </div>
        @foreach($lasted_posts as $lasted_post)
            @if($lasted_post['lang_type'] == \Illuminate\Support\Facades\Config::get('app.locale'))
                <div style="padding:3px 5px;">
                    <img src="{{ asset('assets/site/images/icon/icon4.gif') }}" style="vertical-align:middle;" alt="icon">
                    <a href="{{ route('site.post.detail', $lasted_post['slug']) }}">
                        {{ $lasted_post['title'] }}
                    </a>
                </div>
            @endif
        @endforeach

        <div class="cl"></div>

    </div>
    <div class="divr">
        @include('site.pages.post._list_categories')
        @include('site.layouts.lasted_posts')
    </div>
    <div class="cl"></div>
</div>
