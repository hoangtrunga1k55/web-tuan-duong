@extends('site.layouts.index')
@section('seo')
    <title>{{ $post->title ?? "title" }}</title>
    <meta name="keywords" content="{{ $post->title ?? "title" }}"/>
    <meta name="description" content="{{ $post->title ?? "title" }}"/>
@endsection
@section('content')
    @include("site.layouts.feature_news")
{{--    <div class="dlink">--}}
{{--        <a href="{{ route('site.index') }}">Trang chủ</a>--}}
{{--        &gt;--}}
{{--        <h1><a href="{{ route('site.category', $category->slug) }}">{{ $category->title }}</a></h1>--}}
{{--    </div>--}}
    @include("site.pages.post._main_content")
@endsection
@section('javascript')
    <script>
    </script>
@endsection
