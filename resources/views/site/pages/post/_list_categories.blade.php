<ul class="m">
    <li class="h">{!! __('news') !!}</li>
    @foreach($child_category_news as $child)
    <li @if(\Illuminate\Support\Facades\Route::currentRouteName() =="site.index")) class="h s" @else class="g n"  @endif><a href="{{ route('site.category', $child->slug) }}">{{$child->title}}</a></li>
    @endforeach
</ul>
