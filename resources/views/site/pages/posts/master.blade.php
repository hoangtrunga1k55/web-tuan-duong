@extends('site.layouts.index')
@section('seo')
    <title>{{ $category->title ?? "title" }}</title>
    <meta name="keywords" content="{{ $category->title ?? "title" }}"/>
    <meta name="description" content="{{ $category->title ?? "title" }}"/>
@endsection
@section('content')
    @include("site.layouts.feature_news")
    <div class="dlink">
        <a href="{{ route('site.index') }}">Trang chủ</a>
        &gt;
        <h1><a href="{{ route('site.category', $category->slug) }}">{{ $category->title }}</a></h1>
    </div>
    @include("site.pages.posts._main_content")
@endsection
@section('javascript')
    <script>
    </script>
@endsection
