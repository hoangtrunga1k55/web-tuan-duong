<div class="dhc">
    <div class="dpro">
        <ul id="unew" class="umain">
            @foreach($posts as $post)
                <li @if($loop->first) class="t" @else class="c" @endif>
                    <a href="{{ route('site.post.detail', $post['slug']) }}">
                        <img src="{{ $post->image }}" alt="{{ $post->title }}">
                    </a>
                    <strong>
                        <a href="{{ route('site.post.detail', $post['slug']) }}">{{ $post->title }}</a>
                    </strong>
                    <span>{{ $post->description }}</span>
                    <span>
                        <a class="f" href="{{ route('site.post.detail', $post['slug']) }}" rel="nofollow">{!! __('show_more') !!}</a>
                    </span>
                </li>
            @endforeach
            &nbsp;
            <li class="cl"></li>
        </ul>
        <div class="cl"></div>
    </div>
    <div class="divr">
        @include('site.pages.posts._child_categories')
        @include('site.layouts.lasted_posts')
    </div>
    <div class="cl"></div>
</div>
