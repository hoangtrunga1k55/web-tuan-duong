@if(count($child_categories))
    <ul class="m">
    <li class="h">{{ $category->title }}</li>

    @foreach($child_categories as $category)
        <li class="c  n"><a href="{{ route('site.category', $category->slug) }}">{{ $category->title }}</a></li>
    @endforeach
</ul>
@endif

