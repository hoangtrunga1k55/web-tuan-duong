<div class="dhc">
    <div class="dpro">
        <div class="intro">
            <p>
                <strong>{!! $configs['companyName'] ?? "companyName" !!}</strong>
            </p>
            <p>  {!! __('location') !!}: {!! $configs['location'] ?? "location" !!}</p>
            <p>{{__('phone')}}: {!! $configs['phoneNumber'] ?? "024.66523613" !!}</p>
            <p>{{__('Hotline')}}: {!! $configs['hotline'] ?? "0988 600 898"  !!}</p>
            <p>Website: {{ asset('') }}</p>
            <p>Email: <a href="mailto:{{ $configs['email'] ?? "email" }}">
                    {{ $configs['email'] ?? "email" }}
                </a></p>
        </div>
        <div id="pct">
            <div class="bdt" style="padding-top:10px;">
                {!! __('note') !!}
            </div>

            <div style="padding-bottom:10px;">
                <b>{!! __('note1') !!}:</b>
                {!! __('note_content') !!}
            </div>
            <form action="">
            <table style="width:100%;" cellpadding="0" cellspacing="1">
                <tbody>
                <tr>
                    <td class="l">
                        <div style="width:150px;">{!! __('Fullname') !!} <span class="sao">*</span></div>
                    </td>
                    <td style="width:100%;">
                        <input name="txtNameC" type="text" maxlength="50" id="txtNameC" class="txt12"
                               style="width:350px;">
                        <span id="RequiredFieldValidator11" style="display:none;">*</span>
                    </td>
                </tr>
                <tr>
                    <td class="l">{!! __('address') !!} <span class="sao">*</span></td>
                    <td>
                        <input name="txtAddressC" type="text" maxlength="300" id="txtAddressC" class="txt12"
                               style="width:350px;">
                        <span id="RequiredFieldValidator7" style="display:none;">*</span>
                    </td>
                </tr>
                <tr>
                    <td class="l">{!! __('phone') !!} <span class="sao">*</span></td>
                    <td>
                        <input name="txtTelC" type="text" maxlength="20" id="txtTelC" class="txt12"
                               style="width:350px;">
                        <span id="RequiredFieldValidator9" style="display:none;">*</span>
                    </td>
                </tr>
                <tr>
                    <td class="l">{!! __('email') !!} <span class="sao">*</span></td>
                    <td>
                        <input name="txtEmailC" type="text" maxlength="200" id="txtEmailC" class="txt12"
                               style="width:350px;">
                        <span id="RequiredFieldValidator5" style="display:none;">*</span>
                    </td>
                </tr>
                <tr>
                    <td class="l">{!! __('Contact title') !!} <span class="sao">*</span></td>
                    <td>
                        <input name="NameC" type="text" maxlength="100" id="NameC" class="txt12" style="width:350px;">
                        <span id="RequiredFieldValidator10" style="display:none;">*</span>
                    </td>
                </tr>
                <tr>
                    <td class="l">{!! __('Contact content') !!}</td>
                    <td><textarea name="txtMSc" rows="5" cols="50" id="txtMSc" class="txt12"
                                  style="width:350px;"></textarea></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td style="padding:10px 0;">
                        <input type="button" name="btnCT" value="{!!__("Send")  !!}"
                               onclick="submitContact()" id="btnCT" class="btnID" style="width:100px;">
                        <input style="width:100px;" type="reset" class="btnID" name="cmdReset" value="{!!__("ReFill")  !!}">
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="cl"></div>
    </div>
    <div class="divr">
        @include('site.layouts.lasted_posts')
    </div>
    <div class="cl"></div>
</div>
