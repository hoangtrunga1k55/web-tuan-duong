@extends('site.layouts.index')
@section('seo')
    <title>{{ $configs['companyName'] ?? "companyName" }}</title>
    <meta name="keywords" content="{{ $configs['companyName'] ?? "companyName" }}"/>
    <meta name="description" content="{{ $configs['companyName'] ?? "companyName" }}"/>
@endsection
@section('content')
    @include("site.layouts.feature_news")
    @include("site.pages.contact._main_content")
@endsection

@section('javascript')
    <script
        src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
        crossorigin="anonymous"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function submitContact() {
            let url = `{{ route('site.submit-contact')}}`;

            let data = new FormData();

            data.append('name', $('#txtNameC').val());
            data.append('address', $('#txtAddressC').val());
            data.append('phone', $('#txtTelC').val());
            data.append('email', $('#txtEmailC').val());
            data.append('title', $('#NameC').val());
            data.append('message', $('#txtMSc').val());

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                contentType: false,
                processData: false,
                success: function (response) {
                    alert(response.message);
                },
                error: function (response) {
                    alert(response.responseJSON.message);
                }
            });
        }
    </script>
@endsection

