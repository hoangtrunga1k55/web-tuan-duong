<div class="dhead " @if(\Illuminate\Support\Facades\Config::get('app.locale') == 'us') style="background: url({{'/assets/site/images/bg/tuanduong.png'}}) no-repeat left !important;" @endif>
    <ul>
        <li class="l">
            <a href="{{ route('site.index') }}" title="{{ $configs['companyName'] ?? "companyName" }}"></a>
        </li>
        <li class="r">
            <a href="mailto::{{ $configs['email'] ?? "" }}">
                <b>Email:{!! $configs["email"] ?? "email"  !!}</b>
            </a>
            <hr style="border: 1px solid rgba(246,122,122,0.84)">
            <a href="tel::{{ $configs['hotline'] ?? "0988 600 898" }}">
                <b style="color: #ec1d25">Hotline:{{ $configs['hotline'] ?? "0988 600 898" }}</b>
            </a>
        </li>
    </ul>
</div>
