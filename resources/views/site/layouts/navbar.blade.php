<?php
function check($header,$slug)
{
    $check = 0;
    foreach ($header->getChildNav() as $child) {
        if (isset($slug) && $child->slug == $slug) {
            $check = 1;
            break;
        }
    }
    return $check;
}

?>
<ul class="umh">
    <li @if(\Illuminate\Support\Facades\Route::currentRouteName() =="site.index")) class="h s" @else class="g n"  @endif>
        <a class="a1" href="{{ route('site.index') }}" title="{{ $config['companyName'] ?? "companyName" }}">
            {{(\Illuminate\Support\Facades\Config::get('app.locale')=='vn'?'TRANG CHỦ':'HOMEPAGE')}}
        </a>
    </li>
    @foreach($category_headers as $header)
        @if($header['lang_type'] == \Illuminate\Support\Facades\Config::get('app.locale'))
            <li @if((isset($slug) && check($header,$slug)==1) || (isset($slug) && $slug==$header->slug)) class="h s" @else class="g n"  @endif>
                <a class="a1" href="{{ route('site.category', $header->slug) }}" title="{{ $header->title }}">
                    {{ $header->title }}
                </a>
                <div>
                    @foreach($header->getChildNav() as $child)
                        <a  @if((isset($slug) && $slug==$child->slug)) class="a2 h s" @else class="a2 g n"  @endif href="{{ route('site.category', $child->slug) }}">
                            {{ $child->title }}
                        </a>
                    @endforeach
                </div>
            </li>
        @endif
    @endforeach
    <li @if((\Illuminate\Support\Facades\Route::currentRouteName() =="site.contact"))  class="h s" @else class="g n"  @endif>
        <a class="a1" href="{{(\Illuminate\Support\Facades\Config::get('app.locale')=='vn')?route('site.contact'):route('site.contact_en')}}" rel="nofollow">{{(\Illuminate\Support\Facades\Config::get('app.locale')=='vn')?"LIÊN HỆ":"CONTACT"}}</a>
    </li>
    <li class="g n">
        <a class="a1" href="http://vietlaw.gov.vn/LAWNET/" rel="nofollow">{{(\Illuminate\Support\Facades\Config::get('app.locale')=='vn')?"VĂN BẢN PHÁP LUẬT":"LEGISLATION"}}</a>
    </li>
</ul>
