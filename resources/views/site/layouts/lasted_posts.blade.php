<ul class="t">
    <li class="h">
        {{(\Illuminate\Support\Facades\Config::get('app.locale')=='vn')?'Tin tức cập nhật':'News update'}}
    </li>
    <li class="t">
        <marquee onmouseover="this.stop()" onmouseout="this.start()" scrollamount="3" direction="down">
            @foreach($lasted_posts as $lasted_post)
                @if($lasted_post['lang_type'] == \Illuminate\Support\Facades\Config::get('app.locale'))
                <div>
                    <strong>
                        <a href="{{ route('site.post.detail', $lasted_post['slug']) }}">
                            {{ $lasted_post['title'] }}
                        </a>
                    </strong>
                    <a href="{{ route('site.post.detail', $lasted_post['slug']) }}">
                        <img class="m" src="{{ $lasted_post['image'] }}" alt="{{ $lasted_post['title'] }}"/>
                    </a>
                    <span>{{ $lasted_post['description'] }}</span>
                </div>
                @endif
            @endforeach
        </marquee>
    </li>
</ul>
