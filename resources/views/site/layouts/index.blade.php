<!DOCTYPE html>
<html lang="en">
@include('site.layouts.head')
@yield('css')
<body>
@include('site.layouts.sticktop-navbar')
@include('site.layouts.header')
<div class="content">
    @include('site.layouts.navbar')
    @yield('content')
</div>
@include('site.layouts.footer')
@include('site.layouts.script')
@yield('javascript')
</body>
</html>
