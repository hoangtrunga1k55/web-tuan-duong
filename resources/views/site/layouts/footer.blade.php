<div class="dft">
    <ul class="uft">
        <li class="lfr">
            @foreach($footer as $item)
                @if($item['lang_type']==\Illuminate\Support\Facades\Config::get('app.locale'))
                <a href="{{ $item->link }}" rel="nofollow">{{ $item->name }}</a>
                @endif
            @endforeach
        </li>
        <li class="lfl">
            <div style="line-height:180%; padding-top:5px;">{!! $configs['copyright'] ?? "copyright" !!}<br/>
                {!! $configs['companyName'] ?? "CÔNG TY TNHH THƯƠNG MẠI & ĐẦU TƯ XÂY DỰNG TUẤN DƯƠNG" !!}<br/>
                {!! __('phone') !!}: {!! $configs['phoneNumber'] ?? "024.66523613" !!}<br/>
                {!! __('Hotline') !!}: {!! $configs['hotline'] ?? "0988 600 898"  !!}<br/>
                {!! __('Email') !!}: {!! $configs['email'] ?? "tuanduongcit@gmail.com"  !!}<br/>
                {{__('tax')}}: {!! $configs['tax'] ?? "tax" !!}<br/>
                {{__('location')}}: {!! $configs['location'] ?? "location" !!}<br/>
            </div>
        </li>
    </ul>
    <div class="cl"></div>
</div>
