<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    @yield('seo')
    <script type="text/javascript" src="{{ asset('assets/site/js/jquery-1.js') }}?v=2" charset="UTF-8"></script>
    <script type="text/javascript" src="{{ asset('assets/site/js/mBanner.js') }}?v=2" charset="UTF-8"></script>
    <style type="text/css" media="screen">@import "{{ asset('assets/site/css/main.css') }}?v=2.1";</style>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style type="text/css">
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
        }

        /* Optional: Makes the sample page fill the window. */
        html,
        body {
            height: 100%;
            margin: 0;
        }
    </style>
</head>

