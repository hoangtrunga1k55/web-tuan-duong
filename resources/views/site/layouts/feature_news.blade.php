
<ul class="mslw">
    @foreach($feature_posts as $feature_post)
        @if($feature_post['lang_type'] == \Illuminate\Support\Facades\Config::get('app.locale'))
            <li id="picture{{ $loop->iteration }}">
                <strong>
                    <a href="{{route('site.post.detail',$feature_post['slug'])}}">
                        {{ $feature_post['title'] }}
                    </a>
                </strong>
                 <a href="{{route('site.post.detail',$feature_post['slug'])}}"><span>{{ $feature_post['description'] }}</span></a>
                <div>&nbsp;</div>
                <a href="{{route('site.post.detail',$feature_post['slug'])}}">
                    <img src="{{ $feature_post['image'] }}"
                         alt="{{ $feature_post['title'] }}"/></a>

            </li>
        @endif
    @endforeach

</ul>
<script type="text/javascript" src="{{ asset("assets/site/js/fader.js") }}"></script>
