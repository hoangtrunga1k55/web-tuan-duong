<div class="dtop">
    <ul class="mb-0">
        <li class="t">
            <ul>
                @foreach($languages as $language)
                    @if($language['code']!= 'all')
                        <li style="padding: 0px 3px"><a href="{{route('languageWeb',$language['code'])}}"
                               class="dropdown-item">
                                <img
                                    src="https://flagcdn.com/20x15/{{ $language['code'] }}.png"
                                    alt="{{$language['name']}}">
                            </a>
                        </li>
                    @endif
                @endforeach
            </ul>
        </li>
{{--        <li class="t">--}}
{{--            <a href="?https://twitter.com/share" class="twitter-share-button" rel="nofollow">--}}
{{--                Tweet--}}
{{--            </a>--}}
{{--        </li>--}}
        <li class="f">
            <div class="fb-like" data-send="false" data-layout="button_count" data-width="30"
                 data-show-faces="false"></div>
        </li>
        <li class="c">
            <a href="{{ route('site.index') }}" title="{{ $config['companyName'] ?? "companyName" }}">
                {{(\Illuminate\Support\Facades\Config::get('app.locale')=='vn'?'Trang chủ':'Homepage')}}
            </a>
            {{--            <b>|</b>--}}
            {{--            <a href="?so-do-chi-duong.html" rel="nofollow">Sơ đồ chỉ đường</a>--}}
            <b>|</b>
            <a href="{{(\Illuminate\Support\Facades\Config::get('app.locale')=='vn')?route('site.contact'):route('site.contact_en')}}" rel="nofollow">{{(\Illuminate\Support\Facades\Config::get('app.locale')=='vn')?"Liên hệ":"Contact"}}</a>
            {{--            <b>|</b>--}}
            {{--            <a href="?http://mail.hcci.com.vn:3000/" rel="nofollow" target="_blank">Webmail</a>--}}
        </li>
    </ul>
</div>
