<span id="lblPage">
    <a class="cpage">1</a>
    <a class="apage" href="/tin-tuc/page-2.html">2</a>
    <a class="apage" href="/tin-tuc/page-3.html">3</a>
    <a class="apage" href="/tin-tuc/page-4.html">4</a>
    <a class="apage" href="/tin-tuc/page-5.html">5</a>
    <a class="apage" href="/tin-tuc/page-6.html">6</a>
    <a class="apage" href="/tin-tuc/page-7.html">7</a>
    <a class="apage" href="/tin-tuc/page-8.html">8</a>
    <a class="apage" href="/tin-tuc/page-9.html">9</a>
    <a class="apage" href="/tin-tuc/page-10.html">10</a>
    <a class="apage" href="/tin-tuc/page-2.html">Tiếp</a>
    <a class="apage" href="/tin-tuc/page-12.html">Cuối</a>
</span>

@if ($paginator->hasPages())
    <div id="lblPage">
        <ul class="pagination">
            {{-- Previous Page Link --}}
            @if (!$paginator->onFirstPage())
                <a class="apage" href="{{ $paginator->previousPageUrl() }}l">Trước</a>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <a class="apage">{{ $element }}</a>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <a class="cpage">{{ $page }}</a>
                        @else
                            <a class="apage" href="{{ $url }}">{{ $page }}</a>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <a class="apage" href="{{ $paginator->nextPageUrl() }}">Tiếp</a>
            @endif
        </ul>
    </div>
@endif
