<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title', env('APP_NAME', 'Title') )</title>

    @include('admin.layouts.css')
    <script src="{{asset('ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('ckeditor/ckfinder/ckfinder.js')}}"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('css')
</head>
<body class="hold-transition sidebar-mini" >

<!-- Site wrapper -->
<div class="wrapper">
    @include('admin.layouts.navbar')
    @include('admin.layouts.sidebar')

    <!-- Content Wrapper. Contains page content -->
        @yield('content')
    <!-- /.content-wrapper -->

    @include('admin.layouts.footer')

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

@yield('modals')
@include('admin.layouts.js')
@yield('js')
</body>
</html>
