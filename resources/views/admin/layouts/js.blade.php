<!-- jQuery -->
<script src="{{ asset('assets/admin/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('assets/admin/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/admin/dist/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="/js/app.js?v={{ config('ver.app_js') }}"></script>
<script>
    function confirmDelete(url) {
        Swal.fire({
            title: 'Are you sure?',
            text: "Bạn sẽ không thể khôi phục lại việc này",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
        }).then((result) => {
            if (!!result.isConfirmed) {
                $.ajax({
                    method: "delete",
                    url: url,
                    data: {},
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        Toast.fire({
                            icon: response.status,
                            title: response.message
                        })
                        window.location.href = window.location.href;
                    },
                    error: function (response) {
                        console.log(response);
                        Toast.fire({
                            icon: 'error',
                            title: response.responseJSON.message
                        })
                    }
                });
            }
        })
    }

    function confirmPublish(url) {
        Swal.fire({
            title: 'Are you sure?',
            text: "Bài viết sẽ được công bố",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
        }).then((result) => {
            !!result.isConfirmed ? window.location.href = url : "";
        })
    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 1500,
        timerProgressBar: true
    })

    const loadFile = function (event) {
        let output = document.getElementById("src_" + event.id);
        output.src = URL.createObjectURL(event.files[0]);
        output.onload = function () {
            URL.revokeObjectURL(output.src) // free memory
        }
    };

    $(document).ready(function () {
        $(".form-control.form-control-navbar").on("keyup", function () {
            var value = $(this).val().toLowerCase();
            $("#example2 tr").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });

        function clearForm($form) {
            $form.find(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
            $form.find(':checkbox, :radio').prop('checked', false);
        }

        $("#button_refresh ").click(function (e) {
            clearForm($(this).parents('form'));
            e.preventDefault();
        })
    });
</script>
<script src="/vendor/laravel-filemanager/js/lfm.js"></script>
<script>
    var options = {
        filebrowserImageBrowseUrl: '/filemanage?type=Images&multiple=false'
    };
</script>

<script type="text/javascript">
    $('.lfm').filemanager('image');

    $('.lfm-multi').filemanager('image', {
        multiple: true,
        name: 'slide',
        element: '#slider_container',
        limit: 5
    });
</script>


