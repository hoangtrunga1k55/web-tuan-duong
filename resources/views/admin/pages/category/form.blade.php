@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1244.06px;">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Danh Mục</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <div class="content">
            <div class="content-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <form
                            @if(isset($category))
                            action="{{ route('admin.category.update', [$category->id]) }}"
                            @else
                            action="{{ route('admin.category.store') }}"
                            @endif
                            method="post" role="form" enctype="multipart/form-data"
                            onsubmit="submitForm(this); return false;"
                            class="card card-primary card-outline card-outline-tabs">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="card-header p-0 border-bottom-0">
                                <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="custom-tabs-four-home-tab" data-toggle="pill"
                                           href="#custom-tabs-four-home" role="tab"
                                           aria-controls="custom-tabs-four-home" aria-selected="false" style="">Thông
                                            tin chung</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-four-profile-tab" data-toggle="pill"
                                           href="#custom-tabs-four-profile" role="tab"
                                           aria-controls="custom-tabs-four-profile" aria-selected="false"
                                           style="">SEO</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="custom-tabs-four-tabContent">
                                    <div class="tab-pane fade active show" id="custom-tabs-four-home"
                                         role="tabpanel"
                                         aria-labelledby="custom-tabs-four-home-tab">
                                        <div class="row">
                                            <div class="col-12 col-lg-8">
                                                <div class="row">
                                                    <div class="form-group col-12">
                                                        <label for="title">Tiêu đề</label>
                                                        <input type="text" class="form-control" name="title"
                                                               value="{{ old('title', $category->title ?? "")}}"
                                                               id="title"
                                                               placeholder="Nhập Tiêu Đề">
                                                        <small class="text-danger rule" id="rule-title"></small>
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <label for="decription">Danh mục cha</label>
                                                        <select class="form-control" id="parent_id" name="parent_id">
                                                            <option value="">Chọn danh mục cha</option>
                                                            @if(isset($category->parent_id))
                                                                @foreach($categories as $parent_category)
                                                                    <option
                                                                        value="{{ $parent_category->id }}"
                                                                        {{ old('parent_id', trim(substr($category->parent_id, strripos($category->parent_id,",", -2)), ",")) === $parent_category->id  ? "selected" : null }}>
                                                                        {{ $parent_category->title }}
                                                                    </option>
                                                                @endforeach
                                                            @else
                                                                @foreach($categories as $parent_category)
                                                                    <option
                                                                        value="{{ $parent_category->id }}" {{ old('parent_id') === $parent_category->id ? "selected" : null }}>
                                                                        {{ $parent_category->title }}
                                                                    </option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                        <small class="text-danger rule" id="rule-status"></small>
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <label for="priority">Độ ưu tiên</label>
                                                        <input type="number" class="form-control" name="priority" value="{{old('priority',$category->priority ?? 1)}}">
                                                        <small class="text-danger rule" id="rule-priority"></small>
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <label for="decription">Mô tả</label>
                                                        <input type="text" class="form-control" name="description"
                                                               value="{{ old('description', $category->description ?? "")}}"
                                                               id="description"
                                                               placeholder="Nhập Mô Tả">
                                                        <small class="text-danger rule" id="rule-description"></small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <div class="row">
                                                    @if(isset($languages))
                                                        <div class="form-group col-12">
                                                            <label>Ngôn ngữ</label>
                                                            <select class="form-control" id="lang_type"
                                                                    name="lang_type">
                                                                @if(isset($category->lang_type))
                                                                    @foreach($languages as $language)
                                                                        @if($language['code']!= 'all')
                                                                            <option
                                                                                value="{{$language->code}}" {{ old('lang_type', $category->lang_type) == $language->code  ? "selected" : null }}>
                                                                                {{$language->name}}
                                                                            </option>
                                                                        @endif
                                                                    @endforeach
                                                                @else
                                                                    @foreach($languages as $language)
                                                                        @if($language['code']!= 'all')
                                                                            <option
                                                                                value="{{$language->code}}" {{ old('lang_type') == $language->code  ? "selected" : null }}>
                                                                                {{$language->name}}
                                                                            </option>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                            <small class="text-danger rule" id="rule-lang_type"></small>
                                                        </div>
                                                    @endif
                                                    <div class="form-group col-12">
                                                        <label>Loại danh mục</label>
                                                        <select class="form-control" id="type" name="type">
                                                            @if(isset($category->type))
                                                                @foreach(\App\Models\Category::TYPE as $value=>$type)
                                                                    <option
                                                                        value="{{ $value }}" {{ old('type', $category->type) === $value ? "selected" : null }}>
                                                                        {{ $type }}
                                                                    </option>
                                                                @endforeach
                                                            @else
                                                                @foreach(\App\Models\Category::TYPE as $value=>$type)
                                                                    <option
                                                                        value="{{ $value }}" {{ old('type') === $value ? "selected" : null }}>
                                                                        {{ $type }}
                                                                    </option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                        <small class="text-danger rule" id="rule-status"></small>
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <label>Hiển thị trên navbar</label>
                                                        <select class="form-control" id="show" name="show">
                                                            @if(isset($category->show))
                                                                <option
                                                                    value="0" {{ old('show', $category->show) === "0" ? "selected" : null }}>
                                                                    Ẩn
                                                                </option>
                                                                <option
                                                                    value="1" {{ old('show', $category->show) === "1" ? "selected" : null }}>
                                                                    Hiển thị
                                                                </option>
                                                            @else
                                                                <option
                                                                    value="0" {{ old('show') === "0" ? "selected" : null }}>
                                                                    Ẩn
                                                                </option>
                                                                <option
                                                                    value="1" {{ old('show') === "1" ? "selected" : null }}>
                                                                    Hiển
                                                                    thị
                                                                </option>
                                                            @endif
                                                        </select>
                                                        <small class="text-danger rule" id="rule-status"></small>
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <label>Trạng Thái</label>
                                                        <select class="form-control" id="status" name="status">
                                                            @if(isset($category->status))
                                                                <option
                                                                    value="1" {{ old('status', $category->status) === "1" ? "selected" : null }}>
                                                                    Hiển thị
                                                                </option>
                                                                <option
                                                                    value="0" {{ old('status', $category->status) === "0" ? "selected" : null }}>
                                                                    Ẩn
                                                                </option>
                                                            @else
                                                                <option
                                                                    value="1" {{ old('status') === "1" ? "selected" : null }}>
                                                                    Hiển
                                                                    thị
                                                                </option>
                                                                <option
                                                                    value="0" {{ old('status') === "0" ? "selected" : null }}>
                                                                    Ẩn
                                                                </option>
                                                            @endif
                                                        </select>
                                                        <small class="text-danger rule" id="rule-status"></small>
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <label for="name">Hình Ảnh</label>
                                                        <div class="input-group">
                                                            <span class="input-group-btn">
                                                               <a data-input="image" data-preview="holder"
                                                                  class="lfm btn btn-primary">
                                                                   <i class="fa fa-picture-o"></i> Choose
                                                               </a>
                                                            </span>
                                                            <input id="image" class="form-control" type="text"
                                                                   name="image"
                                                                   value="{{ isset($category->image) ? $category->image : old('image')}}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="custom-tabs-four-profile" role="tabpanel"
                                         aria-labelledby="custom-tabs-four-profile-tab">
                                        <div class="row">

                                            <div class="col-12 col-lg-8">
                                                <div class="row">
                                                    <div class="form-group col-12">
                                                        <label for="meta_title">Meta Title</label>
                                                        <input type="text" class="form-control" name="meta_title"
                                                               id="meta_title" maxlength="70"
                                                               value="{{ old('meta_title', $category->meta_title ?? "") }}"
                                                               placeholder="Nhập Meta Title">
                                                        <small class="text-danger rule"
                                                               id="rule-meta_title"></small>
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <label for="meta_description">Meta Description</label>
                                                        <input type="text" class="form-control"
                                                               name="meta_description"
                                                               id="meta_description" maxlength="160"
                                                               value="{{ old('meta_description', $category->meta_description ?? "") }}"
                                                               placeholder="Nhập Meta Description">
                                                        <small class="text-danger rule"
                                                               id="rule-meta_description"></small>
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <label for="meta_keywords">Meta Keywords</label>
                                                        <input type="text" class="form-control" name="meta_keywords"
                                                               id="meta_keywords" maxlength="160"
                                                               value="{{ old('meta_keywords', $category->meta_keywords ?? "") }}"
                                                               placeholder="Nhập Meta Keywords">
                                                        <small class="text-danger rule"
                                                               id="rule-meta_keywords"></small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <div class="row">
                                                    <div class="form-group col-12">
                                                        <label for="name">Hình Ảnh</label>
                                                        <div class="input-group">
                                                           <span class="input-group-btn">
                                                             <a data-input="meta_thumbnail" data-preview="holder"
                                                                class="lfm btn btn-primary">
                                                               <i class="fa fa-picture-o"></i> Choose
                                                             </a>
                                                           </span>
                                                            <input id="meta_thumbnail" class="form-control" type="text"
                                                                   name="meta_thumbnail"
                                                                   value="{{ isset($category->meta_thumbnail) ? $category->meta_thumbnail : old('meta_thumbnail')}}">
                                                        </div>
                                                    </div>
                                                    <small class="text-danger rule" id="rule-meta_thumbnail"></small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-submit">
                                    @if(!isset($category)) Thêm mới @else Cập nhật @endif
                                </button>
                            </div>
                            <!-- /.card -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script src="{{ asset('assets/admin/custom/ajax-submit.js') }}"></script>
@endsection
