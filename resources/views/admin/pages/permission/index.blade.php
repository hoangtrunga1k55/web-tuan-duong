@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Quyền</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <p>Có tất cả {{ $permissions->total() }} kết quả</p>
                            <table id="example2"
                                   class="table table-striped"
                                   role="grid">
                                <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Tên</th>
                                    <th>Nhóm</th>
                                    <th>Hành Động</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($permissions as $permission)
                                    <tr>
                                        <td>
                                            #{{ ($permissions->currentPage()-1) * $permissions->perPage() + $loop->iteration }}</td>
                                        <td>{{ $permission->name ?? "" }}</td>
                                        <td>{{ $permission->group ?? "" }}</td>
                                        <td>
                                            <select class="form-control" onchange="redirect(this)"
                                                    permission-id="{{ $permission->id }}">
                                                <option readonly>Chọn hành động</option>
                                                @if( \App\Helpers\PermissionsHelper::can('admin.permission.edit'))
                                                    <option
                                                        value="edit">
                                                        Sửa quyền
                                                    </option>
                                                @endif
                                                @if( \App\Helpers\PermissionsHelper::can('admin.permission.destroy'))
                                                    <option
                                                        value="destroy">
                                                        Xóa quyền
                                                    </option>
                                                @endif
                                            </select>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    {{ $permissions->appends(request()->query())->links() }}
                </div>
                <!-- /.card-footer-->
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
@endsection
@section('js')
    <script>

        @if(session('message'))
        Toast.fire({
            icon: 'success',
            title: '{{session('message')}}'
        });
        @endif

        function redirect(event) {
            let res, url;
            let id = event.getAttribute('permission-id');
            switch (event.value) {
                case "edit":
                    url = "{{ route("admin.permission.edit", "%id%") }}";
                    res = url.replace('%id%', id)
                    window.location.href = res;
                    break;
                case "destroy":
                    url = "{{ route("admin.permission.destroy", "%id%") }}";
                    res = url.replace('%id%', id)
                    confirmDelete(res);
                    break;
            }
        }
    </script>
@endsection
