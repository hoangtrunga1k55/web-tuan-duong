@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Menu</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <p>Có tất cả {{ $footers->total() }} kết quả </p>
                            <table id="example2"
                                   class="table table-striped"
                                   role="grid">
                                <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Tên</th>
                                    <th>Độ ưu tiên</th>
                                    <th style="width: 150px">Trạng Thái</th>
                                    <th>Hành Động</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($footers as $footer)
                                    <tr>
                                        <td>
                                            #{{ ($footers->currentPage()-1) * $footers->perPage() + $loop->iteration }}</td>
                                        <td>{{ $footer->name }}</td>
                                        <td>{{ $footer->priority }}</td>
                                        @if($footer->status ==1)
                                            <td>Hiển thị</td>
                                        @else
                                            <td>Ẩn</td>
                                        @endif
                                        <td>
                                            <select class="form-control" onchange="redirect(this)"
                                                    footer-id="{{ $footer->id }}">
                                                <option readonly>Chọn hành động</option>
                                                @if( \App\Helpers\PermissionsHelper::can('admin.footer.edit'))
                                                    <option
                                                        value="edit">
                                                        Sửa
                                                    </option>
                                                @endif
                                                @if( \App\Helpers\PermissionsHelper::can('admin.footer.destroy'))
                                                    <option
                                                        value="destroy">
                                                        Xóa
                                                    </option>
                                                @endif
                                            </select>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                        <!-- /.card-footer-->
                    </div>
                    <!-- /.card -->
                </div>
                <div class="card-footer">
                    {{ $footers->appends(request()->query())->links() }}
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection
@section('js')
    <script>
        @if(session('message'))
        Toast.fire({
            icon: 'success',
            title: '{{session('message')}}'
        });
        @endif

        function redirect(event) {
            let res, url;
            let id = event.getAttribute('footer-id');
            switch (event.value) {
                case "edit":
                    url = "{{ route("admin.footer.edit", "%id%") }}";
                    res = url.replace('%id%', id)
                    window.location.href = res;
                    break;
                case "destroy":
                    url = "{{ route("admin.footer.destroy", "%id%") }}";
                    res = url.replace('%id%', id)
                    confirmDelete(res);
                    break;
            }
        }
    </script>
@endsection
