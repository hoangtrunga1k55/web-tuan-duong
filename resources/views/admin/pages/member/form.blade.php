@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1244.06px;">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Thông tin khách hàng</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <div class="content">
            <div class="content-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{ route('admin.member.update', [$member->id]) }}"
                            method="post" role="form" enctype="multipart/form-data"
                            onsubmit="submitForm(this); return false;"
                            class="card card-primary card-outline card-outline-tabs">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="card-header p-0 border-bottom-0">
                                <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="custom-tabs-four-home-tab" data-toggle="pill"
                                           href="#custom-tabs-four-home" role="tab"
                                           aria-controls="custom-tabs-four-home" aria-selected="false" style="">Thông
                                            tin chung</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="custom-tabs-four-tabContent">
                                    <div class="tab-pane fade active show" id="custom-tabs-four-home"
                                         role="tabpanel"
                                         aria-labelledby="custom-tabs-four-home-tab">
                                        <div class="row">
                                            <div class="col-12 col-lg-8">
                                                <div class="row">
                                                    <div class="form-group col-12">
                                                        <label for="name">Tên khách hàng*</label>
                                                        <input type="text" class="form-control" name="name"
                                                               value="{{ old('name', $member->name ?? "")}}"
                                                               id="name"
                                                               placeholder="Tên khách hàng">
                                                        <small class="text-danger rule" id="rule-name"></small>
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <label for="phone_number">Số điện thoại</label>
                                                        <input type="text" class="form-control" name="phone_number"
                                                               value="{{ old('name', $member->phone_number ?? "")}}"
                                                               id="phone_number"
                                                               placeholder="Nhập số điện thoại">
                                                        <small class="text-danger rule" id="rule-phone_number"></small>
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <label for="phone_number">Email</label>
                                                        <input type="email" class="form-control" name="email"
                                                               value="{{ old('name', $member->email ?? "")}}"
                                                               id="email"
                                                               placeholder="Nhập email">
                                                        <small class="text-danger rule" id="rule-email"></small>
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <label for="address">Địa chỉ</label>
                                                        <input type="text" class="form-control" name="address"
                                                               value="{{ old('name', $member->address ?? "")}}"
                                                               id="address"
                                                               placeholder="Nhập địa chỉ">
                                                        <small class="text-danger rule" id="rule-address"></small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group col-12">
                                                    <label for="name">Hình Ảnh</label>
                                                    <div class="input-group">
                                                           <span class="input-group-btn">
                                                             <a data-input="avatar" data-preview="holder" class="lfm btn btn-primary">
                                                               <i class="fa fa-picture-o"></i> Choose
                                                             </a>
                                                           </span>
                                                        <input id="avatar" class="form-control" type="text" name="avatar" value="{{ isset($member->avatar) ? $member->avatar : old('avatar')}}">
                                                    </div>
                                                    <div class="image-avatar">
                                                        <img  style="margin-top: 20px;margin:auto" width="90%" src="{{ isset($member->avatar) ? $member->avatar : old('avatar')}}" alt="">
                                                    </div>
                                                </div>
                                                <small class="text-danger rule" id="rule-avatar"></small>
                                                <div class="form-group col-12">
                                                    <label for="birth_day">Ngày sinh</label>
                                                    <input type="date" class="form-control" name="birth_day" id="birth_day" value="{{ old('birth_day', isset($member) ? \Carbon\Carbon::createFromTimestamp($member->birth_day)->format('Y-m-d') : "")}}">
                                                    <small class="text-danger rule" id="rule-birth_day"></small>
                                                </div>
                                                @if(\App\Helpers\PermissionsHelper::can('resetPassword'))
                                                <div class="form-group col-12">
                                                    <label for="password">Mật khẩu</label>
                                                    <input type="password" class="form-control" name="password"
                                                           value="{{ old('password', $member->password ?? "")}}"
                                                           id="password"
                                                           placeholder="Nhập password">
                                                    <small class="text-danger rule" id="rule-password"></small>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-submit">
                                    @if(!isset($member)) Thêm mới @else Cập nhật @endif
                                </button>
                            </div>
                            <!-- /.card -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script src="{{ asset('assets/admin/custom/ajax-submit.js') }}"></script>
@endsection
