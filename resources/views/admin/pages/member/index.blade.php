@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1329.44px;">
        <section class="content-header">
            <section class="content">
                <div class="row">
                    <div class="col-12">
                        <h1>Quản lý thành viên</h1>
                        <br>
                        <div class="card">
                            <!-- /.card-header -->
                            <div class="card-body">
                                <form class="row">
                                    <div class="col-6 col-xl-3">
                                        <div class="form-group ">
                                            <label>Email</label>
                                            <input type="text" class="form-control" name="email" id="email"
                                                   placeholder="Nhập Email" value="{{$request->email}}">
                                        </div>
                                    </div>
                                    <div class="col-6 col-xl-3">
                                        <div class="form-group ">
                                            <label>Số điện thoại</label>
                                            <input type="text" class="form-control" name="phone_number" id="phone_number"
                                                   placeholder="Nhập số điện thoại" value="{{$request->phone_number}}">
                                        </div>
                                    </div>
                                    <div class="col-6 col-xl-3">
                                        <div class="form-group ">
                                            <label>Địa chỉ</label>
                                            <input type="text" class="form-control" name="address" id="address"
                                                   placeholder="Nhập địa chỉ" value="{{$request->address}}">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <button type="submit" class="btn btn-success">Tìm kiếm
                                        </button>
                                        <button id="button_refresh" class="btn btn-info">Làm mới</button>
                                    </div>
                                </form>
                                <div class="row pt-5">
                                    <p>Có tất cả {{ $members->total() }} kết quả thỏa mãn điều kiện tìm kiếm</p>
                                    <div class="col-sm-12">
                                        <table id="example2"
                                               class="table table-striped vertical"
                                               role="grid">
                                            <thead>
                                            <tr>
                                                <th style="width: 10px">#</th>
                                                <th>Họ tên</th>
                                                <th>Ảnh đại diện </th>
                                                <th>Ngày sinh</th>
                                                <th>Email</th>
                                                <th>Số điện thoại</th>
                                                <th>Địa chỉ</th>
                                                <th>Hành Động</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($members as $member)
                                                <tr>
                                                    <td>
                                                        {{ ($members->currentPage()-1) * $members->perPage() + $loop->iteration }}.
                                                    </td>
                                                    <td>{{ $member->name }}</td>
                                                    <td><img src="{{ $member->avatar }}" style="width: 75px" alt=""></td>
                                                    <td>{{\Carbon\Carbon::createFromTimestamp($member->birth_day)->format('d-m-Y')}}</td>
                                                    <td>{{ $member->email }}</td>
                                                    <td>{{ $member->phone_number }}</td>
                                                    <td>{{ $member->address }}</td>
                                                    <td>
                                                        <select class="form-control" onchange="redirect(this)"
                                                                member-id="{{ $member->id }}">
                                                            <option>Chọn hành động</option>
                                                            @if( \App\Helpers\PermissionsHelper::can('member.edit'))
                                                                <option
                                                                    value="edit">
                                                                    Sửa tài khoản
                                                                </option>
                                                            @endif
                                                            @if( \App\Helpers\PermissionsHelper::can('member.destroy'))
                                                                <option
                                                                    value="destroy">
                                                                    Xóa tài khoản
                                                                </option>
                                                            @endif
                                                        </select>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                {{ $members->appends(request()->query())->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>
    </div>
@endsection
@section('js')
    <script>
        @if(session('message'))
        Toast.fire({
            icon: 'success',
            title: '{{ session('message') }}'
        });
        @endif

        function redirect(event) {
            let res, url;
            let id = event.getAttribute('member-id');
            switch (event.value) {
                case "edit":
                    url = "{{ route("admin.member.edit", "%id%") }}";
                    res = url.replace('%id%', id)
                    window.location.href = res;
                    break;
                case "destroy":
                    url = "{{ route("admin.member.destroy", "%id%") }}";
                    res = url.replace('%id%', id)
                    console.log(res);
                    confirmDelete(res);
                    break;
            }
        }
    </script>
@endsection


