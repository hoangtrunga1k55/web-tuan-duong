@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1244.06px;">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Banner</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <div class="content">
            <div class="content-fluid">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">@if(isset($banner)) Sửa @else Tạo @endif Banner</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form
                        @if(isset($banner))
                        action="{{ route('admin.banner.update', [$banner->id]) }}"
                        @else
                        action="{{ route('admin.banner.store') }}"
                        @endif
                        onsubmit="submitForm(this); return false;"
                        method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="card-body row">
                            <div class="col-lg-6 col-sm-12">
                                <div class="form-group ">
                                    <label for="title">Tiêu đề</label>
                                    <input type="text" class="form-control" name="title" id="title"
                                           placeholder="Nhập Tiêu đề" value="{{ old('title', $banner->title ?? null)}}">

                                    <small class="text-danger rule"
                                           id="rule-title"></small>
                                </div>
                                <div class="form-group ">
                                    <label for="title">Mô tả ngắn</label>
                                    <input type="text" class="form-control" name="description" id="description"
                                           placeholder="Nhập Mô tả ngắn"
                                           value="{{ old('description',  $banner->description ?? null) }}">

                                    <small class="text-danger rule"
                                           id="rule-description"></small>
                                </div>
                                <div class="form-group ">
                                    <label for="title">Link</label>
                                    <input type="text" class="form-control" name="link" id="link"
                                           placeholder="Nhập Link" value="{{ old('link', $banner->link ?? null) }}">

                                    <small class="text-danger rule"
                                           id="rule-link"></small>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-12">
                                <div class="form-group">
                                    <label>Trạng Thái</label>
                                    <select class="form-control" id="status" name="status">
                                        @if(isset($banner->status))
                                            <option
                                                value="1" {{ old('status', $banner->status) === "1" ? "selected" : null }}>
                                                Hiển thị
                                            </option>
                                            <option
                                                value="0" {{ old('status', $banner->status) === "0" ? "selected" : null }}>
                                                Ẩn
                                            </option>
                                        @else
                                            <option value="1" {{ old('status') === "1" ? "selected" : null }}>Hiển thị
                                            </option>
                                            <option value="0" {{ old('status') === "0" ? "selected" : null }}>Ẩn
                                            </option>
                                        @endif
                                    </select>

                                    <small class="text-danger rule"
                                           id="rule-status"></small>
                                </div>
                                <div class="form-group">
                                    <label for="name">Hình Ảnh</label>
                                    <div class="input-group">
                                       <span class="input-group-btn">
                                         <a data-input="image" data-preview="holder" class="lfm btn btn-primary">
                                           <i class="fa fa-picture-o"></i> Choose
                                         </a>
                                       </span>
                                        <input id="image" class="form-control" type="text" name="image"
                                               value="{{ isset($banner->image) ? $banner->image : old('image')}}">
                                    </div>
                                </div>

                                <small class="text-danger rule"
                                       id="rule-image"></small>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary btn-submit">@if(isset($banner))
                                    Sửa @else Tạo @endif</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ asset('assets/admin/custom/ajax-submit.js') }}"></script>
@endsection
