@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Banner</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <p>Có tất cả {{ $banners->total() }} kết quả </p>
                            <table id="example2"
                                   class="table table-striped"
                                   role="grid">
                                <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Tiêu đề</th>
                                    <th style="width: 150px">Ảnh</th>
                                    <th style="width: 150px">Trạng Thái</th>
                                    <th>Hành Động</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($banners as $banner)
                                    <tr>
                                        <td>
                                            #{{ ($banners->currentPage()-1) * $banners->perPage() + $loop->iteration }}</td>
                                        <td>{{ $banner->title }}</td>
                                        <td><img src="{{ $banner->image }}"
                                                 class="img-thumbnail" alt=""></td>
                                        @if($banner->status ==1)
                                            <td>Hiển thị</td>
                                        @else
                                            <td>Ẩn</td>
                                        @endif
                                        <td>
                                            <select class="form-control" onchange="redirect(this)"
                                                    banner-id="{{ $banner->id }}">
                                                <option readonly>Chọn hành động</option>
                                                @if( \App\Helpers\PermissionsHelper::can('admin.banner.edit'))
                                                    <option
                                                        value="edit">
                                                        Sửa banner
                                                    </option>
                                                @endif
                                                @if( \App\Helpers\PermissionsHelper::can('admin.banner.destroy'))
                                                    <option
                                                        value="destroy">
                                                        Xóa banner
                                                    </option>
                                                @endif
                                            </select>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <div class="card-footer">
                    {{ $banners->appends(request()->query())->links() }}
                </div>
                <!-- /.card-footer-->
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection
@section('js')
    <script>
        @if(session('message'))
        Toast.fire({
            icon: 'success',
            title: '{{session('message')}}'
        });
        @endif

        function redirect(event) {
            let res, url;
            let id = event.getAttribute('banner-id');
            switch (event.value) {
                case "edit":
                    url = "{{ route("admin.banner.edit", "%id%") }}";
                    res = url.replace('%id%', id)
                    window.location.href = res;
                    break;
                case "destroy":
                    url = "{{ route("admin.banner.destroy", "%id%") }}";
                    res = url.replace('%id%', id)
                    confirmDelete(res);
                    break;
            }
        }
    </script>
@endsection
