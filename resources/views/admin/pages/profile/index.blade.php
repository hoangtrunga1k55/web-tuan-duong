@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1244.06px;">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Thông tin cá nhân</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <div class="content">
            <div class="content-fluid">
                <div class="card">
                    <!-- /.card-header -->
                    <!-- form start -->
                    <div class="card-body row">
                        <form action="{{ route('admin.profile.update_info') }}" method="post"
                              onsubmit="submitForm(this); return false;"
                              enctype="multipart/form-data" class="col-12 col-lg-6 border-right">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group ">
                                <label for="title">Họ tên</label>
                                <input type="text" class="form-control" name="full_name" id="full_name"
                                       placeholder="Nhập Họ Tên"
                                       value="{{ old('full_name', $user->full_name ?? "") }}">
                                <small class="text-danger rule" id="rule-full_name"></small>
                            </div>

                            <div class="form-group col-12">
                                <label for="name">Hình Ảnh</label>
                                <div class="input-group">
                                <span class="input-group-btn">
                                   <a data-input="avatar" data-preview="holder"
                                      class="lfm btn btn-primary">
                                       <i class="fa fa-picture-o"></i> Choose
                                   </a>
                                </span>
                                <input id="avatar" class="form-control" type="text"
                                       name="avatar"
                                       value="{{ isset($user->avatar) ? $user->avatar : old('avatar')}}">
                                </div>
                            </div>
                            <button style="margin-top: 30px" type="submit" class="btn btn-primary">Cập nhật</button>
                        </form>
                        <form action="{{ route('admin.profile.update_password') }}" method="post"
                              onsubmit="submitForm(this); return false;"
                              enctype="multipart/form-data" class="col-12 col-lg-6">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group ">
                                <label for="title">Mật khẩu cũ</label>
                                <input type="password" class="form-control" name="old_password" id="old_password"
                                       placeholder="Nhập Mật khẩu cũ">
                                <small class="text-danger rule" id="rule-old_password"></small>
                            </div>
                            <div class="form-group ">
                                <label for="title">Mật khẩu mới</label>
                                <input type="password" class="form-control" name="password" id="password"
                                       placeholder="Nhập Mật khẩu mới">
                                <small class="text-danger rule" id="rule-password"></small>
                            </div>

                            <div class="form-group ">
                                <label for="title">Xác nhận mật khẩu</label>
                                <input type="password" class="form-control" name="password_confirmation"
                                       id="password_confirmation"
                                       placeholder="Nhập Lại Mật khẩu mới">
                                <small class="text-danger rule" id="rule-password_confirmation"></small>
                            </div>
                            <button type="submit" class="btn btn-primary">Cập nhật</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ asset('assets/admin/custom/ajax-submit.js') }}"></script>
@endsection
