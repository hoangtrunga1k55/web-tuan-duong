@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Banner</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <p>Có tất cả {{ $products->total() }} kết quả </p>
                            <table id="example2"
                                   class="table table-striped"
                                   role="grid">
                                <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Ten san pham</th>
                                    <th style="width: 150px">Ảnh</th>
                                    <th style="width: 150px">Trạng Thái</th>
                                    <th>Hành Động</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $product)
                                    <tr>
                                        <td>
                                            #{{ ($products->currentPage()-1) * $products->perPage() + $loop->iteration }}</td>
                                        <td>{{ $product->name }}</td>
                                        <td><img src="{{ $product->thumbnail }}"
                                                 class="img-thumbnail" alt=""></td>
                                        @if($product->status ==1)
                                            <td>Hiển thị</td>
                                        @else
                                            <td>Ẩn</td>
                                        @endif
                                        <td>
                                            <select class="form-control" onchange="redirect(this)"
                                                    product-id="{{ $product->id }}">
                                                <option readonly>Chọn hành động</option>
                                                @if( \App\Helpers\PermissionsHelper::can('admin.product.edit'))
                                                    <option
                                                        value="edit">
                                                        Sửa product
                                                    </option>
                                                @endif
                                                @if( \App\Helpers\PermissionsHelper::can('admin.product.destroy'))
                                                    <option
                                                        value="destroy">
                                                        Xóa product
                                                    </option>
                                                @endif
                                            </select>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                        <!-- /.card-footer-->
                    </div>
                    <!-- /.card -->
                </div>
                <div class="card-footer">
                    {{ $products->appends(request()->query())->links() }}
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection
@section('js')
    <script>
        @if(session('message'))
        Toast.fire({
            icon: 'success',
            title: '{{session('message')}}'
        });
        @endif

        function redirect(event) {
            let res, url;
            let id = event.getAttribute('product-id');
            switch (event.value) {
                case "edit":
                    url = "{{ route("admin.product.edit", "%id%") }}";
                    res = url.replace('%id%', id)
                    window.location.href = res;
                    break;
                case "destroy":
                    url = "{{ route("admin.product.destroy", "%id%") }}";
                    res = url.replace('%id%', id)
                    confirmDelete(res);
                    break;
            }
        }
    </script>
@endsection
