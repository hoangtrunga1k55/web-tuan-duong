@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1244.06px;">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Product</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <div class="content">
            <div class="content-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <form
                            @if(isset($product))
                            action="{{ route('admin.product.update', [$product->id]) }}"
                            @else
                            action="{{ route('admin.product.store') }}"
                            @endif
                            onsubmit="submitForm(this); return false;"
                            method="post" enctype="multipart/form-data"
                            class="card card-primary card-outline card-outline-tabs">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="card-header p-0 border-bottom-0">
                                <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="custom-tabs-four-home-tab" data-toggle="pill"
                                           href="#general" role="tab"
                                           aria-controls="custom-tabs-four-home" aria-selected="false" style="">Thông
                                            tin chung</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-four-home-tab" data-toggle="pill"
                                           href="#images" role="tab"
                                           aria-controls="custom-tabs-four-home" aria-selected="false"
                                           style="">Images</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-four-profile-tab" data-toggle="pill"
                                           href="#custom-tabs-four-profile" role="tab"
                                           aria-controls="custom-tabs-four-profile" aria-selected="false"
                                           style="">SEO</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="custom-tabs-four-tabContent">
                                    <div class="tab-pane fade active show" id="general" role="tabpanel"
                                         aria-labelledby="custom-tabs-four-home-tab">
                                        <div class="row">
                                            <div class="col-lg-6 col-sm-12">
                                                <div class="form-group ">
                                                    <label for="title">Ten san pham</label>
                                                    <input type="text" class="form-control" name="name" id="name"
                                                           placeholder="Product name..."
                                                           value="{{ old('name', $product->name ?? null)}}">
                                                    <small class="text-danger rule"
                                                           id="rule-name"></small>
                                                </div>
                                                <div class="form-group ">
                                                    <label for="title">Product type</label>
                                                    <input type="text" class="form-control" name="type" id="type"
                                                           placeholder="Product type..."
                                                           value="{{ old('type',  $product->type ?? null) }}">
                                                    <small class="text-danger rule"
                                                           id="rule-type"></small>
                                                </div>
                                                <div class="form-group ">
                                                    <label for="sub_type">Product sub type</label>
                                                    <input type="text" class="form-control" name="sub_type"
                                                           id="sub_type"
                                                           placeholder="Product sub type..."
                                                           value="{{ old('sub_type', $product->sub_type ?? null) }}">

                                                    <small class="text-danger rule"
                                                           id="rule-sub_type"></small>
                                                </div>
                                                <div class="form-group ">
                                                    <label for="brand">Product Brand</label>
                                                    <input type="text" class="form-control" name="brand" id="brand"
                                                           placeholder="Product brand..."
                                                           value="{{ old('brand', $product->brand ?? null) }}">

                                                    <small class="text-danger rule"
                                                           id="rule-brand"></small>
                                                </div>
                                                <div class="form-group ">
                                                    <label for="industry">Product Industry</label>
                                                    <input type="text" class="form-control" name="industry"
                                                           id="industry"
                                                           placeholder="Product industry..."
                                                           value="{{ old('industry', $product->industry ?? null) }}">

                                                    <small class="text-danger rule"
                                                           id="rule-industry"></small>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-sm-12">
                                                <div class="form-group">
                                                    <label for="category_ids">Danh Mục</label>
                                                    <div class="select2-primary">
                                                        <select class="form-control select2 select2-dropdown"
                                                                name="category_ids[]"
                                                                id="category_ids" multiple>
                                                            @foreach($categories as $category)
                                                                @if(isset($product))
                                                                    <option
                                                                        value="{{$category->_id }}"
                                                                        {{ in_array($category->_id, old('category_ids', $product->category_ids ?? [])) ?"selected":null}}>
                                                                        {{ $category->title}}
                                                                    </option>
                                                                @else
                                                                    <option value="{{$category->_id}}"
                                                                        {{ in_array($category->_id, old('category_ids', [])) ?"selected":null }}>
                                                                        {{ $category->title }}
                                                                    </option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <small class="text-danger rule" id="rule-category_ids"></small>
                                                </div>
                                                <div class="form-group">
                                                    <label>Trạng Thái</label>
                                                    <select class="form-control" id="status" name="status">
                                                        @if(isset($product->status))
                                                            <option
                                                                value="1" {{ old('status', $product->status) === "1" ? "selected" : null }}>
                                                                Hiển thị
                                                            </option>
                                                            <option
                                                                value="0" {{ old('status', $product->status) === "0" ? "selected" : null }}>
                                                                Ẩn
                                                            </option>
                                                        @else
                                                            <option
                                                                value="1" {{ old('status') === "1" ? "selected" : null }}>
                                                                Hiển thị
                                                            </option>
                                                            <option
                                                                value="0" {{ old('status') === "0" ? "selected" : null }}>
                                                                Ẩn
                                                            </option>
                                                        @endif
                                                    </select>
                                                    <small class="text-danger rule"
                                                           id="rule-status"></small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="images" role="tabpanel"
                                         aria-labelledby="custom-tabs-four-profile-tab">
                                        <div class="row">
                                            <div class="form-group col-lg-4 col-12">
                                                <label for="name">Thumbnail</label>
                                                <div class="input-group">
                                                        <span class="input-group-btn">
                                                            <a data-input="thumbnail" data-preview="holder"
                                                               class="lfm btn btn-primary">
                                                                <i class="fa fa-picture-o"></i> Choose
                                                            </a>
                                                        </span>
                                                    <input id="thumbnail" class="form-control" type="text"
                                                           name="thumbnail"
                                                           value="{{ isset($product->thumbnail) ? $product->thumbnail : old('image')}}">
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-1 col-1">
                                                <label for="slide">slide</label>
                                                <div class="input-group">
                                                    <div class="input-group-btn">
                                                        <a data-input="slide" data-preview="holder"
                                                           class="lfm-multi btn btn-primary">
                                                            <i class="fas fa-plus"></i>
                                                        </a>
                                                    </div>
                                                    <input id="slide" class="form-control" type="text" hidden>
                                                </div>
                                            </div>
                                            <div id="slider_container" class="col-lg-7 col-11 row"></div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="custom-tabs-four-profile" role="tabpanel"
                                         aria-labelledby="custom-tabs-four-profile-tab">
                                        <div class="row">

                                            <div class="col-12 col-lg-8">
                                                <div class="row">
                                                    <div class="form-group col-12">
                                                        <label for="meta_title">Meta Title</label>
                                                        <input type="text" class="form-control" name="meta_title"
                                                               id="meta_title" maxlength="70"
                                                               value="{{ old('meta_title', $product->meta_title ?? "") }}"
                                                               placeholder="Nhập Meta Title">
                                                        <small class="text-danger rule" id="rule-meta_title"></small>
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <label for="meta_description">Meta Description</label>
                                                        <input type="text" class="form-control" name="meta_description"
                                                               id="meta_description" maxlength="160"
                                                               value="{{ old('meta_description', $product->meta_description ?? "") }}"
                                                               placeholder="Nhập Meta Description">
                                                        <small class="text-danger rule"
                                                               id="rule-meta_description"></small>
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <label for="meta_keywords">Meta Keywords</label>
                                                        <input type="text" class="form-control" name="meta_keywords"
                                                               id="meta_keywords" maxlength="160"
                                                               value="{{ old('meta_keywords', $product->meta_keywords ?? "") }}"
                                                               placeholder="Nhập Meta Keywords">
                                                        <small class="text-danger rule" id="rule-meta_keywords"></small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <div class="row">
                                                    <div class="form-group col-12">
                                                        <label for="name">Hình Ảnh</label>
                                                        <div class="input-group">
                                                           <span class="input-group-btn">
                                                             <a data-input="image" data-preview="holder"
                                                                class="lfm btn btn-primary">
                                                               <i class="fa fa-picture-o"></i> Choose
                                                             </a>
                                                           </span>
                                                            <input id="image" class="form-control" type="text"
                                                                   name="meta_thumbnail"
                                                                   value="{{ isset($new->meta_thumbnail) ? $new->meta_thumbnail : old('meta_thumbnail')}}">
                                                        </div>
                                                    </div>
                                                    <small class="text-danger rule" id="rule-meta_thumbnail"></small>
                                                    <img id="src_meta_thumbnail"
                                                         src="{{ old('meta_thumbnail', $product->meta_thumbnail ?? "") }}"
                                                         alt="" class="img-thumbnail">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-submit">
                                    @if(!isset($product)) Thêm mới @else Cập nhật @endif
                                </button>
                            </div>
                            <!-- /.card -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
{{--@section('content')--}}
{{--    <div class="content-wrapper" style="min-height: 1244.06px;">--}}
{{--        <section class="content-header">--}}
{{--            <div class="container-fluid">--}}
{{--                <div class="row mb-2">--}}
{{--                    <div class="col-sm-6">--}}
{{--                        <h1>Product</h1>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div><!-- /.container-fluid -->--}}
{{--        </section>--}}

{{--        <div class="content">--}}
{{--            <div class="content-fluid">--}}
{{--                <div class="card">--}}
{{--                    <div class="card-header">--}}
{{--                        <h3 class="card-title">@if(isset($product)) Create @else Edit @endif Product</h3>--}}
{{--                    </div>--}}
{{--                    <!-- /.card-header -->--}}
{{--                    <!-- form start -->--}}
{{--                    <form--}}
{{--                        @if(isset($product))--}}
{{--                        action="{{ route('admin.product.update', [$product->id]) }}"--}}
{{--                        @else--}}
{{--                        action="{{ route('admin.product.store') }}"--}}
{{--                        @endif--}}
{{--                        onsubmit="submitForm(this); return false;"--}}
{{--                        method="post" enctype="multipart/form-data">--}}
{{--                        <input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
{{--                        <div class="card-body row">--}}
{{--                        </div>--}}
{{--                        <div class="card-footer">--}}
{{--                            <button type="submit" class="btn btn-primary btn-submit">@if(isset($product))--}}
{{--                                    Sửa @else Tạo @endif</button>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

{{--@endsection--}}

@section('js')
    <script src="{{ asset('assets/admin/custom/ajax-submit.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet"
          href="{{ asset('assets/admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <script src="{{ asset('assets/admin/plugins/select2/js/select2.full.min.js') }}"></script>
    <script>
        $('.select2').select2();
        appendSlide();

        function appendSlide() {
            let index = getIndex();
            let html = `
`
            $('#slide-container').append(html);
        }

        function getIndex() {
            let element = document.getElementById("slide-container");
            return element.getElementsByTagName("input").length;
        }
    </script>
@endsection

