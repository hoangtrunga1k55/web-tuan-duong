@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1329.44px;">
        <section class="content-header">
            <section class="content">
                <div class="content-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h1>Danh Sách Tin nhắn</h1>
                            <br>
                            <div class="card">
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <p>Có tất cả {{ $contacts->total() }} kết quả</p>
                                            <table id="example2"
                                                   class="table table-striped"
                                                   role="grid">
                                                <thead>
                                                <tr role="row">
                                                    <th style="width: 10px">#</th>
                                                    <th>Tên</th>
                                                    <th>Email</th>
                                                    <th>Số điện thoại</th>
                                                    <th>Địa chỉ</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($contacts as $contact)
                                                    <tr class="even">
                                                        <td>
                                                            #{{ ($contacts->currentPage()-1) * $contacts->perPage() + $loop->iteration }}</td>
                                                        <td onclick="getInfo('{{ $contact->id }}')">
                                                            <a href="#">{{ $contact->name ?? "" }}</a>
                                                        </td>
                                                        <td>{{ $contact->email ?? "" }}</td>
                                                        <td>{{ $contact->phone ?? "" }}</td>
                                                        <td>{{ $contact->address ?? ""}}</td>


                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    {{ $contacts->appends(request()->query())->links() }}
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>
    </div>

@endsection

@section('modals')
    <!-- Chi tiết tin nhắn -->
    <div class="modal fade" id="detailContact" tabindex="-1" role="dialog" aria-labelledby="detailContact"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Chi tiết tin nhắn</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Tên người gửi: <span id="modal_name"></span><br>
                    Email: <span id="modal_email"></span><br>
                    Số điện thoại: <span id="modal_phone"></span><br>
                    Địa chỉ: <span id="modal_address"></span><br>
                    Nội dung:
                    <p id="modal_content"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        const SCRIPT_REGEX = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi;

        function getInfo(id) {
            $.ajax({
                method: 'post',
                url: '{{ route('admin.contact.store') }}',
                data: {
                    id: id,
                },
                success: function (response) {
                    $("#modal_name").text(response.contact.name);
                    $("#modal_phone").text(response.contact.phone);
                    $("#modal_address").text(response.contact.address);
                    $("#modal_email").text(response.contact.email);
                    let message = response.contact.message;
                    while (SCRIPT_REGEX.test(message)) {
                        message = message.replace(SCRIPT_REGEX, "");
                    }
                    $("#modal_content").html(message);

                    $('#detailContact').modal('show')
                }
            });
        }
    </script>
@endsection
