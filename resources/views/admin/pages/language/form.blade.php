@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1244.06px;">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Ngôn ngữ</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <div class="content">
            <div class="content-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">@if(isset($language)) Sửa @else Tạo @endif Nhà cung cấp</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form @if(isset($language))
                                  action="{{ route('admin.language.update', [$language->id]) }}"
                                  @else
                                  action="{{ route('admin.language.store') }}"
                                  @endif
                                  method="post" role="form"
                                  onsubmit="submitForm(this); return false;"
                                  enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="title">Tên ngôn ngữ</label>
                                        <input type="text" class="form-control" name="name"
                                               value="{{ old('title', $language->name ?? "")}}" id="title"
                                               placeholder="Nhập tên ngôn ngữ">
                                        <small class="text-danger rule" id="rule-name"></small>
                                    </div>
                                    <div class="form-group">
                                        <label for="code">Mã ngôn ngữ</label>
                                        <input type="text" class="form-control" name="code"
                                               value="{{ old('code', $language->code ?? "")}}"
                                               id="code"
                                               placeholder="Nhập mã code">
                                        <small class="text-danger rule" id="rule-code"></small>
                                    </div>
                                    <div class="form-group">
                                        <label for="defalut">Đặt làm mặc định</label>
                                        <input type="hidden" class="form-control" name="default" value="0">
                                        <br>
                                        <input type="checkbox" data-toggle="toggle" class="form-control" name="default" {{$language->default??old('status')?'checked':''}} value="1">
                                    </div>

                                    <div class="form-group">
                                        <label>Trạng Thái</label>
                                        <select class="form-control" id="status" name="status">
                                            @if(isset($language->status))
                                                <option
                                                    value="1" {{ old('status', $language->status) === "1" ? "selected" : null }}>
                                                    Hiển thị
                                                </option>
                                                <option
                                                    value="0" {{ old('status', $language->status) === "0" ? "selected" : null }}>
                                                    Ẩn
                                                </option>
                                            @else
                                                <option value="1" {{ old('status') === "1" ? "selected" : null }}>Hiển
                                                    thị
                                                </option>
                                                <option value="0" {{ old('status') === "0" ? "selected" : null }}>Ẩn
                                                </option>
                                            @endif
                                        </select>
                                        <small class="text-danger rule" id="rule-status"></small>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-submit">@if(isset($language))
                                            Sửa @else Tạo @endif</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script src="{{ asset('assets/admin/custom/ajax-submit.js') }}"></script>
@endsection
