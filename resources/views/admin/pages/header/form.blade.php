@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1244.06px;">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Menu</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <div class="content">
            <div class="content-fluid">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">@if(isset($header)) Sửa @else Tạo @endif Menu</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form
                        @if(isset($header))
                        action="{{ route('admin.header.update', [$header->id]) }}"
                        @else
                        action="{{ route('admin.header.store') }}"
                        @endif
                        onsubmit="submitForm(this); return false;"
                        method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="card-body row">
                            <div class="col-lg-6 col-sm-12">
                                <div class="form-group ">
                                    <label for="name">Tên</label>
                                    <input type="text" class="form-control" name="name" id="name"
                                           placeholder="Nhập tên" value="{{ old('name', $header->name ?? null)}}">

                                    <small class="text-danger rule"
                                           id="rule-name"></small>
                                </div>
                                <div class="form-group ">
                                    <label for="title">Link</label>
                                    <input type="text" class="form-control" name="link" id="link"
                                           placeholder="Nhập Link" value="{{ old('link', $header->link ?? null) }}">

                                    <small class="text-danger rule"
                                           id="rule-link"></small>
                                </div>
                                <div class="form-group ">
                                    <label for="title">Độ ưu tiên</label>
                                    <input type="number" class="form-control" name="priority" id="priority"
                                           placeholder="Độ ưu tiến" value="{{ old('priority', $header->priority ?? null) }}">

                                    <small class="text-danger rule"
                                           id="rule-priority"></small>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-12">
                                @if(isset($languages))
                                    <div class="form-group col-12">
                                        <label>Ngôn ngữ</label>
                                        <select class="form-control" id="lang_type" name="lang_type">
                                            @if(isset($header->lang_type))
                                                @foreach($languages as $language)
                                                    @if($language['code']!= 'all')
                                                    <option
                                                        value="{{$language->code}}" {{ old('lang_type', $header->lang_type) == $language->code  ? "selected" : null }}>
                                                        {{$language->name}}
                                                    </option>
                                                    @endif
                                                @endforeach
                                            @else
                                                @foreach($languages as $language)
                                                    @if($language['code']!= 'all')
                                                    <option
                                                        value="{{$language->code}}" {{ old('lang_type') == $language->code  ? "selected" : null }}>
                                                        {{$language->name}}
                                                    </option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                        <small class="text-danger rule" id="rule-lang_type"></small>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label>Trạng Thái</label>
                                    <select class="form-control" id="status" name="status">
                                        @if(isset($header->status))
                                            <option
                                                value="1" {{ old('status', $header->status) === "1" ? "selected" : null }}>
                                                Hiển thị
                                            </option>
                                            <option
                                                value="0" {{ old('status', $header->status) === "0" ? "selected" : null }}>
                                                Ẩn
                                            </option>
                                        @else
                                            <option value="1" {{ old('status') === "1" ? "selected" : null }}>Hiển thị
                                            </option>
                                            <option value="0" {{ old('status') === "0" ? "selected" : null }}>Ẩn
                                            </option>
                                        @endif
                                    </select>

                                    <small class="text-danger rule"
                                           id="rule-status"></small>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ asset('assets/admin/custom/ajax-submit.js') }}"></script>
@endsection
