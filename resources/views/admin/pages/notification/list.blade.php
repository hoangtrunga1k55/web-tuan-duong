@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1329.44px;">
        <section class="content-header">
            <section class="content">
                <div class="content-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h1>Danh Sách Thông báo</h1>
                            <br>
                            <form class="card collapsed-card form-horizontal" method="get" id="search">
                                <div class="card-header">
                                    <h3 class="card-title">Tìm kiếm</h3>
                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                                class="fas fa-plus"></i>
                                        </button>
                                    </div>
                                    <!-- /.card-tools -->
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body" style="display: none;">
                                    <div class="row">
                                        <div class="col-lg-3 col-6">
                                            <!-- text input -->
                                            <div class="form-group">
                                                <label for="status">Trạng thái</label>
                                                <select name="status" id="filter_status" class="form-control">
                                                    <option value="">Trạng thái</option>
                                                    <option value="1" {{ ($request->status == 1) ? 'selected' : '' }}>
                                                        Hiển thị
                                                    </option>
                                                    <option value="0" {{ ($request->status == 0) ? 'selected' : '' }}>
                                                        Ẩn
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.card-footer -->
                                </div>
                                <div class="card-footer">
                                    <button type="submit" form="search" class="btn btn-success">Tìm kiếm</button>
                                    <button id="button_refresh" class="btn btn-info">Làm mới</button>
                                </div>
                                <!-- /.card-body -->
                            </form>

                            <div class="card">
                                <div class="card-header">
                                    <div class="card-tools">
                                        @if(\App\Helpers\PermissionsHelper::can('admin.notification.create'))
                                            <a href="{{ route('admin.notification.create') }}" type="button"
                                               class="btn btn-outline-success">
                                                Thêm Mới
                                            </a>
                                        @endif
                                    </div>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6"></div>
                                            <div class="col-sm-12 col-md-6"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <table id="example2" class="table table-striped vertical"
                                                       role="grid">
                                                    <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Tiêu Đề</th>
                                                        <th>Mô Tả</th>
                                                        <th>Trạng Thái</th>
                                                        <th>Topic</th>
                                                        <th>Khách</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($notifications as $notification)
                                                        <tr class="even">
                                                            <td>{{ ($notifications->currentPage()-1) * $notifications->perPage() + $loop->iteration }}</td>
                                                            <td>{{ $notification->title }}</td>
                                                            <td>{{ $notification->body }}</td>
                                                            @if($notification->status == 1)
                                                                <td>Thành công</td>
                                                            @else
                                                                <td>Thất bại</td>
                                                            @endif
                                                            <td>{{ $notification->topic ?? "-" }}</td>
                                                            <td>{{ $notification->user->name ?? "-" }}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    {{ $notifications->appends(request()->query())->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>
    </div>
@endsection
@section('js')
    <script>
        @if(session('message'))
        Toast.fire({
            icon: 'success',
            title: '{{ session('message') }}'
        });
        @endif
    </script>
@endsection


