@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Thẻ</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="card">
                <div class="card-body">
                    <div class="row">
{{--                    <div class="row pt-5">--}}
                        <div class="col-sm-12">
                            <p>Có tất cả {{ $tags->total() }} kết quả </p>
                            <table id="example2"
                                   class="table table-striped"
                                   role="grid">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Tên Thẻ</th>
                                    <th>Mô Tả</th>
                                    <th>Trạng Thái</th>
                                    <th>Hành Động</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($tags as $tag)
                                    <tr>
                                        <td>#{{ ($tags->currentPage()-1) * $tags->perPage() + $loop->iteration }}</td>
                                        <td>{{ $tag->name ?? "" }}</td>
                                        <td>{{ $tag->description ?? "" }}</td>
                                        <td>
                                            @if($tag->status ==1)
                                                <span class="badge badge-success">Hiển thị</span>
                                            @else
                                                <span class="badge badge-success">Ẩn</span>
                                            @endif
                                        </td>
                                        <td>
                                            <select class="form-control" onchange="redirect(this)"
                                                    category-id="{{ $tag->id }}">
                                                <option readonly>Chọn hành động</option>
                                                @if( \App\Helpers\PermissionsHelper::can('admin.tag.edit'))
                                                    <option
                                                        value="edit">
                                                        Sửa thẻ
                                                    </option>
                                                @endif
                                                @if( \App\Helpers\PermissionsHelper::can('admin.tag.destroy'))
                                                    <option
                                                        value="destroy">
                                                        Xóa thẻ
                                                    </option>
                                                @endif
                                            </select>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    {{ $tags->appends(request()->query())->links() }}
                </div>
                <!-- /.card-footer-->
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
@endsection
@section('js')
    <script>
        @if(session('message'))
        Toast.fire({
            icon: 'success',
            title: '{{ session('message') }}'
        });
        @endif

        function redirect(event) {
            let res, url;
            let id = event.getAttribute('category-id');
            switch (event.value) {
                case "edit":
                    url = "{{ route("admin.tag.edit", "%id%") }}";
                    res = url.replace('%id%', id)
                    window.location.href = res;
                    break;
                case "destroy":
                    url = "{{ route("admin.tag.destroy", "%id%") }}";
                    res = url.replace('%id%', id)
                    confirmDelete(res);
                    break;
            }
        }
    </script>
@endsection
