@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1244.06px;">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Thẻ</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <div class="content">
            <div class="content-fluid">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">@if(isset($tag)) Sửa @else Tạo @endif Danh Mục Thẻ</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form @if(isset($tag))
                          action="{{ route('admin.tag.update', [$tag->id]) }}"
                          @else
                          action="{{ route('admin.tag.store') }}"
                          @endif
                          method="post"
                          onsubmit="submitForm(this); return false;"
                          enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="card-body row">
                            <div class="col-lg-6 col-sm-12">
                                <div class="form-group ">
                                    <label for="title">Tên Nhãn</label>
                                    <input type="text" class="form-control" name="name" id="name"
                                           placeholder="Nhập Tên" value="{{ old('name', $tag->name ?? "") }}">
                                </div>
                                <small class="text-danger rule"
                                       id="rule-name"></small>
                                <div class="form-group ">
                                    <label for="title">Mô tả ngắn</label>
                                    <input type="text" class="form-control" name="description" id="description"
                                           placeholder="Nhập Mô tả ngắn"
                                           value="{{ old('description', $tag->description ?? "") }}">
                                </div>
                                <small class="text-danger rule"
                                       id="rule-description"></small>
                            </div>
                            <div class="col-lg-3 col-sm-12">
                                <div class="form-group">
                                    <label>Trạng Thái</label>
                                    <select class="form-control" id="status" name="status">
                                        @if(isset($tag->status))
                                            <option
                                                value="1" {{ old('status', $tag->status) === "1" ? "selected" : null }}>
                                                Hiển
                                                thị
                                            </option>
                                            <option
                                                value="0" {{ old('status', $tag->status) === "0" ? "selected" : null }}>
                                                Ẩn
                                            </option>
                                        @else
                                            <option value="1" {{ old('status') === "1" ? "selected" : null }}>Hiển thị
                                            </option>
                                            <option value="0" {{ old('status') === "0" ? "selected" : null }}>Ẩn
                                            </option>
                                        @endif
                                    </select>
                                </div>
                                <small class="text-danger rule"
                                       id="rule-status"></small>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">@if(isset($tag)) Sửa @else Tạo @endif</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ asset('assets/admin/custom/ajax-submit.js') }}"></script>
@endsection
