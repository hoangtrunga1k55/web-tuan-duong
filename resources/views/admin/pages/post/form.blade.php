<?

use Illuminate\Support\Facades\Auth;

?>
@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1244.06px;">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Tin Tức</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <div class="content">
            <div class="content-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <form
                            @if(isset($post))
                            action="{{ route('admin.post.update', [$post->id]) }}"
                            @else
                            action="{{ route('admin.post.store') }}"
                            @endif
                            onsubmit="submitForm(this); return false;"
                            method="post" enctype="multipart/form-data"
                            class="card card-primary card-outline card-outline-tabs">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="card-header p-0 border-bottom-0">
                                <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="custom-tabs-four-home-tab" data-toggle="pill"
                                           href="#custom-tabs-four-home" role="tab"
                                           aria-controls="custom-tabs-four-home" aria-selected="false" style="">Thông
                                            tin chung</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-four-profile-tab" data-toggle="pill"
                                           href="#custom-tabs-four-profile" role="tab"
                                           aria-controls="custom-tabs-four-profile" aria-selected="false"
                                           style="">SEO</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="custom-tabs-four-tabContent">
                                    <div class="tab-pane fade active show" id="custom-tabs-four-home" role="tabpanel"
                                         aria-labelledby="custom-tabs-four-home-tab">
                                        <div class="row">
                                            <div class="col-12 col-lg-8">
                                                <div class="row">
                                                    <div class="form-group col-12">
                                                        <label for="title">Tiêu Đề</label>
                                                        <input type="text" class="form-control" name="title" id="title"
                                                               value="{{ old('title', $post->title ?? "") }}"
                                                               placeholder="Nhập Tiêu Đề">
                                                        <small class="text-danger rule" id="rule-title"></small>
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <label for="description">Mô Tả</label>
                                                        <input type="text" class="form-control" name="description"
                                                               id="description"
                                                               value="{{ old('description', $post->description ?? "") }}"
                                                               placeholder="Nhập Mô Tả">
                                                        <small class="text-danger rule" id="rule-description"></small>
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <label for="status">Nội Dung</label><br>
                                                        <small class="text-danger rule" id="rule-content"></small>
                                                        <textarea name="content"
                                                                  id="content">{{ old('content', $post->content ?? "") }}</textarea>
                                                    </div>
                                                    <script>
                                                        let editor = CKEDITOR.replace('content');
                                                        CKEDITOR.config.extraPlugins = "toc";
                                                    </script>
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <div class="row">
                                                    @if(isset($languages))
                                                        <div class="form-group col-12">
                                                            <label>Ngôn ngữ</label>
                                                            <select class="form-control" id="lang_type" name="lang_type">
                                                                @if(isset($post->lang_type))
                                                                    @foreach($languages as $language)
                                                                        @if($language['code']!= 'all')
                                                                        <option
                                                                            value="{{$language->code}}" {{ old('lang_type', $post->lang_type) == $language->code  ? "selected" : null }}>
                                                                            {{$language->name}}
                                                                        </option>
                                                                        @endif
                                                                    @endforeach
                                                                @else
                                                                    @foreach($languages as $language)
                                                                        @if($language['code']!= 'all')
                                                                        <option
                                                                            value="{{$language->code}}" {{ old('lang_type') == $language->code  ? "selected" : null }}>
                                                                            {{$language->name}}
                                                                        </option>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            </select>
                                                            <small class="text-danger rule" id="rule-lang_type"></small>
                                                        </div>
                                                    @endif
                                                    <div class="form-group col-12">
                                                        <label>Hiển thị trên banner?</label>
                                                        <select class="form-control" id="feature_post" name="feature_post">
                                                            @if(isset($post->feature_post))
                                                                <option
                                                                    value="0" {{ old('feature_post', $post->feature_post) === "0" ? "selected" : null }}>
                                                                    Ẩn
                                                                </option>
                                                                <option
                                                                    value="1" {{ old('feature_post', $post->feature_post) === "1" ? "selected" : null }}>
                                                                    Hiển thị
                                                                </option>
                                                            @else
                                                                <option
                                                                    value="0" {{ old('feature_post') === "0" ? "selected" : null }}>
                                                                    Ẩn
                                                                </option>
                                                                <option
                                                                    value="1" {{ old('feature_post') === "1" ? "selected" : null }}>
                                                                    Hiển thị
                                                                </option>
                                                            @endif
                                                        </select>
                                                        <small class="text-danger rule"
                                                               id="rule-feature_post"></small>
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <label for="priority">Độ ưu tiên</label>
                                                        <input type="number" class="form-control" name="priority"
                                                               id="priority"
                                                               value="{{ old('priority', $post->priority ?? "") }}">
                                                        <small class="text-danger rule" id="rule-priority"></small>
                                                    </div>
                                                    @if(\App\Helpers\PermissionsHelper::can('admin.post.publish'))
                                                        <div class="form-group col-12">
                                                            <label for="priority">Thời gian công bố</label>
                                                            <input type="datetime-local" class="form-control"
                                                                   name="published_at"
                                                                   id="published_at"
                                                                   value="{{ old('published_at', isset($post) ? $post->published_at->format('Y-m-d\TH:i') : "" )}}">
                                                            <small class="text-danger rule"
                                                                   id="rule-published_at"></small>
                                                        </div>
                                                    @endif
                                                    <div class="form-group col-12">
                                                        <label for="category_ids">Danh Mục</label>
                                                        <div class="select2-pink">
                                                            <select class="form-control select2 select2-dropdown"
                                                                    name="category_ids[]"
                                                                    id="category_ids" multiple>
                                                                @foreach($categories as $category)
                                                                    @if(isset($post))
                                                                        <option
                                                                            value="{{$category->_id }}"
                                                                            {{ in_array($category->_id, old('category_ids', $post->category_ids ?? [])) ?"selected":null}}>
                                                                            {{ $category->title}}
                                                                        </option>
                                                                    @else
                                                                        <option value="{{$category->_id}}"
                                                                            {{ in_array($category->_id, old('category_ids', [])) ?"selected":null }}>
                                                                            {{$category->title}}
                                                                        </option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <small class="text-danger rule" id="rule-category_ids"></small>
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <label for="tag_ids">Tag</label>
                                                        <div class="select2-pink">
                                                            <select class="tag form-control select2" name="tag_ids[]"
                                                                    id="tag_ids"
                                                                    multiple="multiple">
                                                                @foreach($tags as $tag)
                                                                    @if(isset($post))
                                                                        <option
                                                                            value="{{$tag->_id }}"
                                                                            {{ in_array($tag->_id, old('tag_ids', $post->tag_ids ?? [])) ?"selected":null}}>
                                                                            {{ $tag->name}}
                                                                        </option>
                                                                    @else
                                                                        <option value="{{$tag->_id}}"
                                                                            {{ in_array($tag->_id, old('tag_ids', [])) ?"selected":null }}>
                                                                            {{$tag->name}}
                                                                        </option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <small class="text-danger rule" id="rule-tag_ids"></small>
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <label for="vendor_ids">Vendor</label>
                                                        <div class="select2-pink">
                                                            <select class="tag form-control select2" name="vendor_ids[]"
                                                                    id="vendor_ids"
                                                                    multiple="multiple">
                                                                @foreach($vendors as $vendor)
                                                                    @if(isset($post))
                                                                        <option
                                                                            value="{{$vendor->_id }}"
                                                                            {{ in_array($vendor->_id, old('vendor_ids', $post->tag_ids ?? [])) ?"selected":null}}>
                                                                            {{ $vendor->name ?? ""}}
                                                                        </option>
                                                                    @else
                                                                        <option value="{{$vendor->_id}}"
                                                                            {{ in_array($vendor->_id, old('vendor_ids', [])) ?"selected":null }}>
                                                                            {{$vendor->name ?? ""}}
                                                                        </option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <small class="text-danger rule" id="rule-vendor_ids"></small>
                                                    </div>

                                                    <div class="form-group col-12">
                                                        <label for="name">Hình Ảnh</label>
                                                        <div class="input-group">
                                                           <span class="input-group-btn">
                                                             <a data-input="image" data-preview="holder" class="lfm btn btn-primary">
                                                               <i class="fa fa-picture-o"></i> Choose
                                                             </a>
                                                           </span>
                                                            <input id="image" class="form-control" type="text" name="image" value="{{ isset($post->image) ? $post->image : old('image')}}">
                                                        </div>
                                                    </div>
                                                    <small class="text-danger rule" id="rule-image"></small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="custom-tabs-four-profile" role="tabpanel"
                                         aria-labelledby="custom-tabs-four-profile-tab">
                                        <div class="row">

                                            <div class="col-12 col-lg-8">
                                                <div class="row">
                                                    <div class="form-group col-12">
                                                        <label for="meta_title">Meta Title</label>
                                                        <input type="text" class="form-control" name="meta_title"
                                                               id="meta_title" maxlength="70"
                                                               value="{{ old('meta_title', $post->meta_title ?? "") }}"
                                                               placeholder="Nhập Meta Title">
                                                        <small class="text-danger rule" id="rule-meta_title"></small>
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <label for="meta_description">Meta Description</label>
                                                        <input type="text" class="form-control" name="meta_description"
                                                               id="meta_description" maxlength="160"
                                                               value="{{ old('meta_description', $post->meta_description ?? "") }}"
                                                               placeholder="Nhập Meta Description">
                                                        <small class="text-danger rule"
                                                               id="rule-meta_description"></small>
                                                    </div>
                                                    <div class="form-group col-12">
                                                        <label for="meta_keywords">Meta Keywords</label>
                                                        <input type="text" class="form-control" name="meta_keywords"
                                                               id="meta_keywords" maxlength="160"
                                                               value="{{ old('meta_keywords', $post->meta_keywords ?? "") }}"
                                                               placeholder="Nhập Meta Keywords">
                                                        <small class="text-danger rule" id="rule-meta_keywords"></small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <div class="row">
                                                    <div class="form-group col-12">
                                                        <label for="name">Hình Ảnh</label>
                                                        <div class="input-group">
                                                           <span class="input-group-btn">
                                                             <a data-input="image" data-preview="holder" class="lfm btn btn-primary">
                                                               <i class="fa fa-picture-o"></i> Choose
                                                             </a>
                                                           </span>
                                                            <input id="image" class="form-control" type="text" name="meta_thumbnail" value="{{ isset($post->meta_thumbnail) ? $post->meta_thumbnail : old('meta_thumbnail')}}">
                                                        </div>
                                                    </div>
                                                    <small class="text-danger rule" id="rule-meta_thumbnail"></small>
                                                    <img id="src_meta_thumbnail"
                                                         src="{{ old('meta_thumbnail', $post->meta_thumbnail ?? "") }}"
                                                         alt="" class="img-thumbnail">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-submit">
                                    @if(!isset($post)) Thêm mới @else Cập nhật @endif
                                </button>
                                @if(isset($post))
                                    @if( \App\Helpers\PermissionsHelper::can('admin.post.approve'))
                                        <button class="btn btn-success btn-modal" posts-id="{{ $post->id }}"
                                                onclick="changeStatus(this)"
                                                value="{{ \App\Models\Post::WAIT_RELEASE }}">
                                            Duyệt bài viết
                                        </button>
                                    @endif
                                    @if( \App\Helpers\PermissionsHelper::can('admin.post.publish'))
                                        <button class="btn btn-success btn-modal" posts-id="{{ $post->id }}"
                                                onclick="changeStatus(this)"
                                                value="{{ \App\Models\Post::RELEASE }}">
                                            Công bố bài viết
                                        </button>
                                    @endif
                                @endif
                            </div>
                            <!-- /.card -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('assets/admin/custom/ajax-submit.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet"
          href="{{ asset('assets/admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <script src="{{ asset('assets/admin/plugins/select2/js/select2.full.min.js') }}"></script>
    <script>
        $('.select2').select2();

        function changeStatus(event) {
            $(event).prop('disabled', true);
            console.log(event.getAttribute('posts-id'), event.value)
            $.ajax({
                method: "POST",
                url: "{{ route('admin.post.update-status') }}",
                data: {
                    id: event.getAttribute('posts-id'),
                    status: event.value,
                },
                success: function (data) {
                    $(event).prop('disabled', false);
                    Toast.fire({
                        icon: data.status,
                        title: data.message
                    }).then(function () {
                        location.reload();
                    });
                },
                error: function (data) {
                    $(event).prop('disabled', false);
                    Toast.fire({
                        icon: "error",
                        title: data.responseJSON.message
                    });
                }
            })
        }
    </script>
    <script>
        $("select.tag ").select2({
            tags: true,
        })
    </script>
@endsection
