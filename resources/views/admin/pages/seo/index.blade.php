@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Thông tin SEO</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <p>Có tất cả {{ $seo_info->total() }} kết quả </p>
                            <table id="example2"
                                   class="table table-striped"
                                   role="grid">
                                <thead>
                                <tr>
                                    <th style="width: 10px">#</th>
                                    <th>Tên trang</th>
                                    <th>Hành Động</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($seo_info as $info)
                                    <tr>
                                        <td>
                                            #{{ ($seo_info->currentPage()-1) * $seo_info->perPage() + $loop->iteration }}</td>
                                        <td>{{ $info->site_name }}</td>
                                        <td>
                                            <select class="form-control" onchange="redirect(this)"
                                                    info-id="{{ $info->id }}">
                                                <option readonly>Chọn hành động</option>
                                                @if( \App\Helpers\PermissionsHelper::can('admin.seo.edit'))
                                                    <option
                                                        value="edit">
                                                        Sửa
                                                    </option>
                                                @endif
                                                @if( \App\Helpers\PermissionsHelper::can('admin.seo.destroy'))
                                                    <option
                                                        value="destroy">
                                                        Xóa
                                                    </option>
                                                @endif
                                            </select>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <div class="card-footer">
                    {{ $seo_info->appends(request()->query())->links() }}
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection
@section('js')
    <script>
        @if(session('message'))
        Toast.fire({
            icon: 'success',
            title: '{{session('message')}}'
        });
        @endif

        function redirect(event) {
            let res, url;
            let id = event.getAttribute('info-id');
            switch (event.value) {
                case "edit":
                    url = "{{ route("admin.seo.edit", "%id%") }}";
                    res = url.replace('%id%', id)
                    window.location.href = res;
                    break;
                case "destroy":
                    url = "{{ route("admin.seo.destroy", "%id%") }}";
                    res = url.replace('%id%', id)
                    confirmDelete(res);
                    break;
            }
        }
    </script>
@endsection
