@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1244.06px;">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Thông tin SEO</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <div class="content">
            <div class="content-fluid">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">@if(isset($header)) Sửa @else Tạo @endif Thông tin SEO </h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form
                        @if(isset($info))
                        action="{{ route('admin.seo.update', $info->id) }}"
                        @else
                        action="{{ route('admin.seo.store') }}"
                        @endif
                        onsubmit="submitForm(this); return false;"
                        method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="card-body row">
                            <div class="col-12 col-lg-8">
                                <div class="row">
                                    <div class="form-group col-12">
                                        <label for="title">Meta Title</label>
                                        <input type="text" class="form-control" name="title"
                                               id="title" maxlength="70"
                                               value="{{ old('title', $info->title ?? "") }}"
                                               placeholder="Nhập Meta Title">
                                        <small class="text-danger rule"
                                               id="rule-title"></small>
                                    </div>
                                    <div class="form-group col-12">
                                        <label for="description">Meta Description</label>
                                        <input type="text" class="form-control"
                                               name="description"
                                               id="description" maxlength="160"
                                               value="{{ old('description', $info->description ?? "") }}"
                                               placeholder="Nhập Meta Description">
                                        <small class="text-danger rule"
                                               id="rule-description"></small>
                                    </div>
                                    <div class="form-group col-12">
                                        <label for="keywords">Meta Keywords</label>
                                        <input type="text" class="form-control" name="keywords"
                                               id="keywords"
                                               value="{{ old('keywords', $info->keywords ?? "") }}"
                                               placeholder="Nhập Meta Keywords">
                                        <small class="text-danger rule"
                                               id="rule-keywords"></small>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4">
                                <div class="row">
                                    <div class="form-group col-12">
                                        <label for="keywords">Tên trang</label>
                                        <input type="text" class="form-control" name="site_name"
                                               id="site_name"
                                               value="{{ old('site_name', $info->site_name ?? "") }}"
                                               placeholder="Nhập Tên trang">
                                        <small class="text-danger rule"
                                               id="rule-site_name"></small>
                                    </div>
                                    <div class="form-group col-12">
                                        <label>Route</label><br>
                                        <small class="text-danger rule" id="rule-route"></small>
                                        <div class="input-group">
                                            <select class="form-control" onchange="updateHref()" id="route" name="route">
                                                @foreach($routes as $route)
                                                    <option
                                                        value="{{ $route['name'] }}" {{ old('route', $info->route ?? null) == $route['name'] ? "selected" : null }}>{{ asset($route['uri']) }}</option>
                                                @endforeach
                                            </select>
                                            <span class="input-group-append">
                                            <a href="#" target="_blank" id="btn-href" type="button" class="btn btn-info" title="Tiến đến trang được chọn">
                                               <i class="fas fa-arrow-right"></i>
                                            </a>
                                        </span>
                                        </div>
                                    </div>
                                    <div class="form-group col-12">
                                        <label for="name">Hình Ảnh</label>
                                        <div class="input-group">
                                       <span class="input-group-btn">
                                         <a data-input="image" data-preview="holder" class="lfm btn btn-primary">
                                           <i class="fa fa-picture-o"></i> Choose
                                         </a>
                                       </span>
                                            <input id="image" class="form-control" type="text" name="image"
                                                   value="{{ isset($info->image) ? $info->image : old('image')}}">
                                        </div>
                                    </div>

                                    <small class="text-danger rule"
                                           id="rule-image"></small>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">
                                @if(!isset($info)) Thêm mới @else Cập nhật @endif
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script src="{{ asset('assets/admin/custom/ajax-submit.js') }}"></script>
    <script>
        updateHref();
        function updateHref() {
            let url = $("#route option:selected").text();
            $('#btn-href').attr('href', url)

        }
    </script>
@endsection
