@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1244.06px;">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Danh Mục</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <div class="content">
            <div class="content-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-primary card-outline card-outline-tabs">
                            <div class="card-header p-0 border-bottom-0">
                                <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="custom-tabs-four-home-tab" data-toggle="pill"
                                           href="#about" role="tab"
                                           aria-controls="custom-tabs-four-home" aria-selected="false" style="">GIỚI THIỆU CHUNG</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-four-home-tab" data-toggle="pill"
                                           href="#section_1" role="tab"
                                           aria-controls="custom-tabs-four-home" aria-selected="false" style="">Section 1</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-four-home-tab" data-toggle="pill"
                                           href="#section_2" role="tab"
                                           aria-controls="custom-tabs-four-home" aria-selected="false" style="">Section 2</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-four-home-tab" data-toggle="pill"
                                           href="#section_3" role="tab"
                                           aria-controls="custom-tabs-four-home" aria-selected="false" style="">Section 3</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-four-home-tab" data-toggle="pill"
                                           href="#section_4" role="tab"
                                           aria-controls="custom-tabs-four-home" aria-selected="false" style="">Section 4</a>
                                    </li>
{{--                                    <li class="nav-item">--}}
{{--                                        <a class="nav-link" id="custom-tabs-four-home-tab" data-toggle="pill"--}}
{{--                                           href="#section_5" role="tab"--}}
{{--                                           aria-controls="custom-tabs-four-home" aria-selected="false" style="">Section 5</a>--}}
{{--                                    </li>--}}
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-four-home-tab" data-toggle="pill"
                                           href="#about_en" role="tab"
                                           aria-controls="custom-tabs-four-home" aria-selected="false" style="">GIỚI THIỆU CHUNG(en)</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-four-home-tab" data-toggle="pill"
                                           href="#section_1_en" role="tab"
                                           aria-controls="custom-tabs-four-home" aria-selected="false" style="">Section 1(en)</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-four-home-tab" data-toggle="pill"
                                           href="#section_2_en" role="tab"
                                           aria-controls="custom-tabs-four-home" aria-selected="false" style="">Section 2(en)</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-four-home-tab" data-toggle="pill"
                                           href="#section_3_en" role="tab"
                                           aria-controls="custom-tabs-four-home" aria-selected="false" style="">Section 3(en)</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-four-home-tab" data-toggle="pill"
                                           href="#section_4_en" role="tab"
                                           aria-controls="custom-tabs-four-home" aria-selected="false" style="">Section 4(en)</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="custom-tabs-four-tabContent">
                                    <div class="tab-pane fade active show" id="about"
                                         role="tabpanel"
                                         aria-labelledby="custom-tabs-four-home-tab">
                                        @include('admin.pages.homepage.section', ['number' => "about", 'section' => $section_about,'languages' =>$languages])
                                    </div>
                                    <div class="tab-pane fade show" id="section_1"
                                         role="tabpanel"
                                         aria-labelledby="custom-tabs-four-home-tab">
                                        @include('admin.pages.homepage.section', ['number' => 1, 'section' => $section_1,'languages' =>$languages])
                                    </div>
                                    <div class="tab-pane fade" id="section_2" role="tabpanel"
                                         aria-labelledby="section_2">
                                        @include('admin.pages.homepage.section', ['number' => 2, 'section' => $section_2,'languages' =>$languages])
                                    </div>
                                    <div class="tab-pane fade" id="section_3" role="tabpanel"
                                         aria-labelledby="section_3">
                                        @include('admin.pages.homepage.section', ['number' => 3, 'section' => $section_3,'languages' =>$languages])
                                    </div>
                                    <div class="tab-pane fade" id="section_4" role="tabpanel"
                                         aria-labelledby="section_4">
                                        @include('admin.pages.homepage.section', ['number' => 4, 'section' => $section_4,'languages' =>$languages])
                                    </div>
{{--                                    <div class="tab-pane fade" id="section_5" role="tabpanel"--}}
{{--                                         aria-labelledby="section_5">--}}
{{--                                        @include('admin.pages.homepage.section', ['number' => 5, 'section' => $section_5])--}}
{{--                                    </div>--}}
                                    <div class="tab-pane fade" id="about_en" role="tabpanel"
                                         aria-labelledby="about_en">
                                        @include('admin.pages.homepage.section', ['number' => 'about_en','section'=>$section_about_en,'languages' =>$languages])
                                    </div>

                                    <div class="tab-pane fade show" id="section_1_en"
                                         role="tabpanel"
                                         aria-labelledby="section_1_en">
                                        @include('admin.pages.homepage.section', ['number' => '1_en','languages' =>$languages])
                                    </div>
                                    <div class="tab-pane fade" id="section_2_en" role="tabpanel"
                                         aria-labelledby="section_2_en">
                                        @include('admin.pages.homepage.section', ['number' => '2_en','languages' =>$languages])
                                    </div>
                                    <div class="tab-pane fade" id="section_3_en" role="tabpanel"
                                         aria-labelledby="section_3_en">
                                        @include('admin.pages.homepage.section', ['number' => '3_en','languages' =>$languages])
                                    </div>
                                    <div class="tab-pane fade" id="section_4_en" role="tabpanel"
                                         aria-labelledby="section_4_en">
                                        @include('admin.pages.homepage.section', ['number' => '4_en','languages' =>$languages])
                                    </div>
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script src="{{ asset('assets/admin/custom/ajax-submit.js') }}"></script>
    <link rel="stylesheet" type="text/css"
          href="{{ asset('assets/admin/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.css') }}">
    <script src="{{ asset('assets/admin/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.js') }}"></script>
    <script>
        $('.duallistbox').bootstrapDualListbox();
    </script>
@endsection
