<form action="{{ route('admin.homepage.store') }}"
      method="post" role="form" enctype="multipart/form-data"
      onsubmit="submitForm(this); return false;">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="section" value="{{ $number }}">
    <div class="row">
        <div class="form-group col-12">
            <label for="title">Tiêu đề</label>
            <input type="text" class="form-control" name="title"
                   value="{{ old('title', $section->title ?? "")}}"
                   id="title{{ $number }}"
                   placeholder="Nhập Tiêu Đề">
            <small class="text-danger rule" id="rule-title"></small>
        </div>
        <div class="form-group col-12">
            <label for="decription">Danh mục</label>
            <select class="form-control" id="category_id{{ $number }}" name="category_id">
                <option value="">Chọn danh mục cha</option>
                @if(isset($section->category_id))
                    @foreach($categories as $category)
                        <option
                            value="{{ $category->id }}"
                            {{ old('category_id', $section->category_id ?? "") === $category->id  ? "selected" : null }}>
                            {{ $category->title }}
                        </option>
                    @endforeach
                @else
                    @foreach($categories as $category)
                        <option
                            value="{{ $category->id }}" {{ old('category_id') === $category->id ? "selected" : null }}>
                            {{ $category->title }}
                        </option>
                    @endforeach
                @endif
            </select>
            <small class="text-danger rule" id="rule-status"></small>
        </div>
        <div class="form-group col-12">
            <label>Ngôn ngữ</label>
            <select class="form-control" id="lang_type"
                    name="lang_type">
                @if(isset($category->lang_type))
                    @foreach($languages as $language)
                        @if($language['code']!= 'all')
                            <option
                                value="{{$language->code}}" {{ old('lang_type', $category->lang_type) == $language->code  ? "selected" : null }}>
                                {{$language->name}}
                            </option>
                        @endif
                    @endforeach
                @else
                    @foreach($languages as $language)
                        @if($language['code']!= 'all')
                            <option
                                value="{{$language->code}}" {{ old('lang_type') == $language->code  ? "selected" : null }}>
                                {{$language->name}}
                            </option>
                        @endif
                    @endforeach
                @endif
            </select>
            <small class="text-danger rule" id="rule-lang_type"></small>
        </div>
        <div class="form-group col-12">
            <label for="post_ids{{ $number }}">Bài viết</label>
            <select class="duallistbox" multiple="multiple" name="post_ids[]" id="post_ids{{ $number }}">
                @if(isset($section))
                    @foreach($posts as $post)
                        <option
                            value="{{ $post->id }}" {{ in_array($post->id, $section->post_ids ?? []) ? "selected" : null }}>{{ $post->title ?? "-"  }}</option>
                    @endforeach
                @else
                    @foreach($posts as $post)
                        <option value="{{ $post->id }}">{{ $post->title ?? "-" }}</option>
                    @endforeach
                @endif
            </select>
        </div>
    </div>
    <button type="submit" class="btn btn-primary btn-submit">
         Cập nhật
    </button>
</form>
