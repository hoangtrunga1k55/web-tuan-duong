@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1329.44px;">
        <section class="content-header">
            <section class="content">
                <div class="content-fluid">
                    <div class="row">
                        <div class="col-12">
                            <h1>Danh Sách Cấu Hình</h1>
                            <br>
                            <div class="card">
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <form class="row">
                                        <div class="col-6 col-xl-3">
                                            <div class="form-group ">
                                                <label>Từ khóa</label>
                                                <input type="text" class="form-control" name="key" id="key"
                                                       placeholder="Nhập tiêu đề" value="{{$request->key}}">
                                            </div>
                                        </div>
                                        <div class="col-6 col-xl-3">
                                            <div class="form-group">
                                                <label for="filter_status">Ngôn ngữ</label>
                                                <select name="lang_type" id="filter_status" class="form-control">
                                                    <option value="">Chọn ngôn ngữ</option>
                                                    @foreach($languages as $language)
                                                        <option
                                                            value="{{$language['code']}}" {{($request->lang_type == $language['code'])?'selected':''}}>
                                                            {{$language['name']}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-6 col-xl-3">
                                            <div class="form-group">
                                                <label for="filter_status">Ngôn ngữ</label>
                                                <select name="code" id="filter_status" class="form-control">
                                                    <option value="">Chọn trạng thái</option>
                                                    @foreach($languages as $language)
                                                    <option value="{{$language->code}}">
                                                        {{$language->name}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <button type="submit" class="btn btn-success">Tìm kiếm
                                            </button>
                                            <button id="button_refresh" class="btn btn-info">Làm mới</button>
                                        </div>
                                    </form>
                                    <br>
                                    <div class="row">
                                        <p>Có tất cả {{ $configs->total() }} kết quả thỏa mãn</p>
                                        <div class="col-sm-12">
                                            <table id="example2"
                                                   class="table table-striped"
                                                   role="grid">
                                                <thead>
                                                <tr role="row">
                                                    <th>#</th>
                                                    <th>Tiêu Đề</th>
                                                    <th> Key</th>
                                                    <th>Ngôn ngữ</th>
                                                    <th> Trạng Thái</th>
                                                    <th> Hành Động</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($configs as $config)
                                                    <tr class="even">
                                                        <td>#{{ ($configs->currentPage()-1) * $configs->perPage() + $loop->iteration }}</td>
                                                        <td>{{$config->title ?? ""}}</td>
                                                        <td>{{$config->key ?? ""}}</td>
                                                        <td>{{$config->lang_type ?? ""}}</td>
                                                        @if($config->status ==1)
                                                            <td>Hiển thị</td>
                                                        @else
                                                            <td>Ẩn</td>
                                                        @endif
                                                        <td>
                                                            <select class="form-control" onchange="redirect(this)"
                                                                    config-id="{{ $config->id }}">
                                                                <option readonly>Chọn hành động</option>
                                                                @if( \App\Helpers\PermissionsHelper::can('admin.config.edit'))
                                                                    <option
                                                                        value="edit">
                                                                        Sửa cấu hình
                                                                    </option>
                                                                @endif
                                                                @if( \App\Helpers\PermissionsHelper::can('admin.config.destroy'))
                                                                    <option
                                                                        value="destroy">
                                                                        Xóa cấu hình
                                                                    </option>
                                                                @endif
                                                            </select>
                                                        </td>

                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    {{ $configs->appends(request()->query())->links() }}
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
            </section>
        </section>
    </div>
@endsection
@section('js')
    <script>
        @if(session('message'))
        Toast.fire({
            icon: 'success',
            title: '{{session('message')}}'
        });
        @endif

        function redirect(event) {
            let res, url;
            let id = event.getAttribute('config-id');
            switch (event.value) {
                case "edit":
                    url = "{{ route("admin.config.edit", "%id%") }}";
                    res = url.replace('%id%', id)
                    window.location.href = res;
                    break;
                case "destroy":
                    url = "{{ route("admin.config.destroy", "%id%") }}";
                    res = url.replace('%id%', id)
                    confirmDelete(res);
                    break;
            }
        }
    </script>
@endsection


