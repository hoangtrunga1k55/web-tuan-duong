@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Danh Sách Nhà Cung Cấp</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="card">
                <div class="card-body">
                    <form class="row">
                        <div class="col-6 col-xl-3">
                            <div class="form-group ">
                                <label for="status">Trạng thái</label>
                                <select name="status" id="filter_status" class="form-control">
                                    <option value="">Trạng thái</option>
                                    <option value="1" {{ ($request->status == 1) ? 'selected' : '' }}>Hiển thị</option>
                                    <option
                                        value="0" {{($request->status == 0 && !is_null($request->status))?'selected':''}}>
                                        Ẩn
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <button type="submit" class="btn btn-success">Tìm kiếm
                            </button>
                            <button id="button_refresh" class="btn btn-info">Làm mới</button>
                        </div>
                    </form>
                    <div class="row pt-5">
                        <div class="col-sm-12">
                            <p>Có tất cả {{ $vendors->total() }} kết quả</p>
                            <table id="example2"
                                   class="table table-striped"
                                   role="grid">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Tiêu Đề</th>
                                    <th>Hình Ảnh</th>
                                    <th>Mô Tả</th>
                                    <th>Trạng Thái</th>
                                    <th>Hành Động</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($vendors as $vendor)
                                    <tr>
                                        <td>{{ ($vendors->currentPage()-1) * $vendors->perPage() + $loop->iteration }}</td>
                                        <td>{{$vendor->title}}</td>
                                        <td><img
                                                src="{{ $vendor->image }}"
                                                height="50px" width="50px" alt=""></td>
                                        <td>{{$vendor->description}}</td>
                                        @if($vendor->status ==1)
                                            <td>Hiển thị</td>
                                        @else
                                            <td>Ẩn</td>
                                        @endif
                                        <td>
                                            <select class="form-control" onchange="redirect(this)"
                                                    vendor-id="{{ $vendor->id }}">
                                                <option readonly>Chọn hành động</option>
                                                @if( \App\Helpers\PermissionsHelper::can('admin.role.edit'))
                                                    <option
                                                        value="edit">
                                                        Sửa Nhà Cung Cấp
                                                    </option>
                                                @endif
                                                @if( \App\Helpers\PermissionsHelper::can('admin.role.destroy'))
                                                    <option
                                                        value="destroy">
                                                        Xóa Nhà Cung Cấp
                                                    </option>
                                                @endif
                                            </select>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    {{ $vendors->appends(request()->query())->links() }}
                </div>
                <!-- /.card-footer-->
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
@endsection
@section('js')
    <script>
        @if(session('message'))
        Toast.fire({
            icon: 'success',
            title: '{{session('message')}}'
        });
        @endif

        function redirect(event) {
            let res, url;
            let id = event.getAttribute('vendor-id');
            switch (event.value) {
                case "edit":
                    url = "{{ route("admin.vendor.edit", "%id%") }}";
                    res = url.replace('%id%', id)
                    window.location.href = res;
                    break;
                case "destroy":
                    url = "{{ route("admin.vendor.destroy", "%id%") }}";
                    res = url.replace('%id%', id)
                    confirmDelete(res);
                    break;
            }
        }
    </script>
@endsection
