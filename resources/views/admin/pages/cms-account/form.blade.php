@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper" style="min-height: 1244.06px;">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Cms Account</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <div class="content">
            <div class="content-fluid">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">@if(isset($cmsAccount)) Sửa @else Tạo @endif Cms Account</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form
                        @if(isset($cmsAccount))
                        action="{{ route('admin.cms-account.update', [$cmsAccount->id]) }}"
                        @else
                        action="{{ route('admin.cms-account.store') }}"
                        @endif
                        method="post"
                        onsubmit="submitForm(this); return false;"
                        enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="card-body row">
                            <div class="col-6">
                                <div class="form-group ">
                                    <label for="full_name">Họ tên</label>
                                    <input type="text" class="form-control" name="full_name" id="full_name"
                                           placeholder="Nhập Họ và tên"
                                           value="{{ old('full_name', $cmsAccount->full_name ?? null) }}">
                                    <small class="text-danger rule" id="rule-full_name"></small>
                                </div>
                                <div class="form-group ">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" name="email" id="email"
                                           placeholder="Nhập Email"
                                           value="{{ old('email', $cmsAccount->email ?? null) }}">
                                    <small class="text-danger rule" id="rule-email"></small>
                                </div>
                                @if(!isset($cmsAccount))
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control" name="password" id="password"
                                               placeholder="Nhập Mật Khẩu">
                                        <small class="text-danger rule" id="rule-password"></small>
                                    </div>
                                @endif
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label for="roles">Roles</label>
                                    <select class="form-control select2" id="roles" name="roles[]" multiple>

                                        @if(isset($cmsAccount->roles) && $cmsAccount->roles->toArray()) != [])
                                        @foreach($roles as $role)
                                            <option
                                                value="{{ $role->id }}"
                                                {{ in_array($role->id, old('roles', $cmsAccount->role_ids)) ? "selected" : null }}>
                                                {{ $role->name}}
                                            </option>
                                        @endforeach
                                        @else
                                            @foreach($roles as $role)
                                                <option
                                                    {{ in_array($role->id, old('roles', [])) ? "selected" : null }}
                                                    value="{{ $role->id }}">{{ $role->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    <small class="text-danger rule" id="rule-roles"></small>
                                </div>
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <select class="form-control" id="status" name="status">
                                        @if(isset($cmsAccount->status))
                                            <option value="1" {{ $cmsAccount->status === "1" ? "selected" : null }}>
                                                Active
                                            </option>
                                            <option value="0" {{ $cmsAccount->status === "0" ? "selected" : null }}>
                                                Suspend
                                            </option>
                                        @else
                                            <option value="1">Active</option>
                                            <option value="0">Suspend</option>
                                        @endif
                                    </select>
                                    <small class="text-danger rule" id="rule-status"></small>
                                </div>
                                <div class="form-group col-12">
                                    <label for="name">Hình Ảnh</label>
                                    <div class="input-group">
                                                           <span class="input-group-btn">
                                                             <a data-input="image" data-preview="holder" class="lfm btn btn-primary">
                                                               <i class="fa fa-picture-o"></i> Choose
                                                             </a>
                                                           </span>
                                        <input id="image" class="form-control" type="text" name="image" value="{{ isset($cmsAccount->image) ? $cmsAccount->image : old('image')}}">
                                    </div>
                                </div>
                                <small class="text-danger rule" id="rule-image"></small>
                            </div>

                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary btn-submit">Submit</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ asset('assets/admin/custom/ajax-submit.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/admin/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet"
          href="{{ asset('assets/admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
    <script src="{{ asset('assets/admin/plugins/select2/js/select2.full.min.js') }}"></script>
    <script>
        $('.select2').select2()
    </script>
@endsection
