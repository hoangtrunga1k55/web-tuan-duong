@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Danh sách người dùng</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="card">
                <div class="card-body">
                    <form class="row">
                        <div class="col-6 col-xl-3">
                            <div class="form-group ">
                                <label>Email</label>
                                <input type="text" class="form-control" name="email" id="email"
                                       placeholder="Nhập email" value="{{$request->email}}">
                            </div>
                        </div>
                        <div class="col-6 col-xl-3">
                            <div class="form-group ">
                                <label>Họ tên</label>
                                <input type="text" class="form-control" name="name" id="name"
                                       placeholder="Nhập họ tên" value="{{$request->name}}">
                            </div>
                        </div>
                        <div class="col-6 col-xl-3">
                            <div class="form-group">
                                <label for="filter_status">Trạng thái</label>
                                <select name="status" id="filter_status" class="form-control">
                                    <option value="">Chọn trạng thái</option>
                                    <option
                                        value="1" {{($request->status == 1)?'selected':''}}>
                                        Đang hoạt động
                                    </option>
                                    <option
                                        value="0" {{($request->status == 0 && !is_null($request->status))?'selected':''}}>
                                        Ngừng hoạt động
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <button type="submit" class="btn btn-success">Tìm kiếm
                            </button>
                            <button id="button_refresh" class="btn btn-info">Làm mới</button>
                        </div>
                    </form>
                    <div class="row pt-5">
                        <p>Có tất cả {{ $cmsAccounts->total() }} kết quả thỏa mãn điều kiện tìm kiếm</p>
                        <table id="example2"
                               class="table table-striped"
                               role="grid">
                            <thead>
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Họ Tên</th>
                                <th>Email</th>
                                <th>Trạng Thái</th>
                                <th>Nhóm quyền</th>
                                <th>Hành Động</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($cmsAccounts as $cmsAccount)
                                <tr>
                                    <td>
                                        #{{ ($cmsAccounts->currentPage()-1) * $cmsAccounts->perPage() + $loop->iteration }}
                                    </td>
                                    <td>{{ $cmsAccount->full_name ?? "" }}</td>
                                    <td>{{ $cmsAccount->email ?? "" }}</td>
                                    <td>
                                    <span class="badge badge-{{ $cmsAccount->getBadgeColor() }}">
                                        {{ $cmsAccount->getStatus() }}
                                    </span>
                                    </td>
                                    <td>
                                        <details>
                                            <summary>Danh sách nhóm quyền</summary>
                                            <ul class="list-unstyled">
                                                @foreach($cmsAccount->roles as $role)
                                                    <li>{{ $role->name ?? ""}}@if(!$loop->last), @endif</li>
                                                @endforeach
                                            </ul>
                                        </details>
                                    </td>
                                    <td>
                                        <select class="form-control" onchange="redirect(this)"
                                                category-id="{{ $cmsAccount->id }}">
                                            <option readonly>Chọn hành động</option>
                                            @if( \App\Helpers\PermissionsHelper::can('admin.cms-account.edit'))
                                                <option
                                                    value="edit">
                                                    Sửa tài khoản
                                                </option>
                                            @endif
                                            @if( \App\Helpers\PermissionsHelper::can('admin.cms-account.destroy'))
                                                <option
                                                    value="destroy">
                                                    Xóa tài khoản
                                                </option>
                                            @endif
                                        </select>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    {{ $cmsAccounts->appends(request()->query())->links() }}
                </div>
                <!-- /.card-footer-->
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')
    <script>
        @if(session('message'))
        Toast.fire({
            icon: 'success',
            title: '{{ session('message') }}'
        });
        @endif

        function redirect(event) {
            let res, url;
            let id = event.getAttribute('category-id');
            switch (event.value) {
                case "edit":
                    url = "{{ route("admin.cms-account.edit", "%id%") }}";
                    res = url.replace('%id%', id)
                    window.location.href = res;
                    break;
                case "destroy":
                    url = "{{ route("admin.cms-account.destroy", "%id%") }}";
                    res = url.replace('%id%', id)
                    confirmDelete(res);
                    break;
            }
        }
    </script>
@endsection
