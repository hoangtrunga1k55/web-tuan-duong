@extends('admin.layouts.master')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Gói bán hàng</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="card">
                <div class="card-body">
                    <p>Có tất cả {{ $packages->total() }} kết quả</p>
                    <table id="example2"
                           class="table table-striped"
                           role="grid">
                        <thead>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Tên Gói</th>
                            <th style="width: 150px">Mô Tả</th>
                            <th style="width: 150px">Trạng Thái</th>
                            <th colspan="2">Hành Động</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($packages as $package)
                            <tr>
                                <td>
                                    #{{ ($packages->currentPage()-1) * $packages->perPage() + $loop->iteration }}</td>
                                <td>{{ $package->name ?? "" }}</td>
                                <td>{{ $package->desc ?? "" }}</td>
                                @if($package->status ==1)
                                    <td>Hiển thị</td>
                                @else
                                    <td>Ẩn</td>
                                @endif
                                <td>
                                    <select class="form-control" onchange="redirect(this)"
                                            package-id="{{ $package->id }}">
                                        <option readonly>Chọn hành động</option>
                                        @if( \App\Helpers\PermissionsHelper::can('admin.package.edit'))
                                            <option
                                                value="edit">
                                                Sửa gói bán hàng
                                            </option>
                                        @endif
                                        @if( \App\Helpers\PermissionsHelper::can('admin.package.destroy'))
                                            <option
                                                value="destroy">
                                                Xóa gói bán hàng
                                            </option>
                                        @endif
                                    </select>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    {{ $packages->appends(request()->query())->links() }}
                </div>
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
@endsection
@section('js')
    <script>
        @if(session('message'))
        Toast.fire({
            icon: 'success',
            title: '{{session('message')}}'
        });
        @endif

        function redirect(event) {
            let res, url;
            let id = event.getAttribute('package-id');
            switch (event.value) {
                case "edit":
                    url = "{{ route("admin.package.edit", "%id%") }}";
                    res = url.replace('%id%', id)
                    window.location.href = res;
                    break;
                case "destroy":
                    url = "{{ route("admin.package.destroy", "%id%") }}";
                    res = url.replace('%id%', id)
                    confirmDelete(res);
                    break;
            }
        }
    </script>
@endsection
