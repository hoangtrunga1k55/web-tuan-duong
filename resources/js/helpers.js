import axios from "axios";
import _ from "lodash";
import moment from "moment";

export default {
    encodeQueryData: function (data) {
        const ret = [];
        for (let d in data)
            if (data[d]) {
                ret.push(encodeURIComponent(d) + '=' + encodeURI(data[d]));
            }
        return ret.join('&');
    },
    getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1) ) + min;
    },
    requestAPI: async (path, method = 'GET', params = {}, header = {}, timeout = 120000) => {
        let baseURL = path.indexOf('http') === -1 ? APP_URL : '';
        let additionPath = path;

        if (method == 'GET') {
            var query = Object.keys(params).map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k])).join('&');
            additionPath = `${additionPath}?${query}`;
        }
        try {
            const CancelToken = axios.CancelToken;
            const source = CancelToken.source();
            const response = await axios.request({
                url: additionPath,
                method: method,
                baseURL: baseURL,
                headers: header,
                data: (method === 'POST' || method === 'PATCH') ? params : null,
                timeout: timeout,
                responseType: 'json',
                responseEncoding: 'utf8',
                cancelToken: source.token
            });
            console.log(method, baseURL + '/' + additionPath, params);
            console.log(response);
            return response.status === 200 ? response.data : {};
        }
        catch (e) {
            console.log(method, baseURL + '/' + additionPath, params, e.message);
            return {};
        }
    },
    requestAPIWithoutServer: async (path, method = 'GET', params = {}, headers = {}, timeout = 120000) => {
        let additionPath = path;

        if (method == 'GET') {
            var query = Object.keys(params).map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k])).join('&');
            additionPath = `${additionPath}?${query}`;
        }
        try {
            const CancelToken = axios.CancelToken;
            const source = CancelToken.source();
            const response = await axios.request({
                url: additionPath,
                method: method,
                headers,
                data: method === 'POST' ? params : null,
                timeout: timeout,
                responseType: 'json',
                responseEncoding: 'utf8',
                cancelToken: source.token
            });
            console.log(method, additionPath, params);
            console.log(response);

            return response.status === 200 ? response.data : {};
        }
        catch (e) {
            console.log(method, additionPath, params, e.message);
            return {};
        }
    },
    v: (value, element = '', defaultValue) => {
        try {
            if (_.isNil(value) || _.isNull(value)) return defaultValue;
            if (element.length === 0) {
                if (_.isString(defaultValue) && _.isString(value)) return value;
                if (_.isArray(defaultValue) && _.isArray(value)) return [...value];
                if (_.isObject(defaultValue) && _.isObject(value)) return {...value};
                if (_.isNumber(defaultValue) && _.isNumber(value)) return value;
                if (_.isBoolean(defaultValue) && _.isBoolean(value)) return value;
            } else {
                let check = {...value};
                const split = element.split('.');
                for (let i = 0; i < split.length; i++) {
                    check = check[split[i]];
                    if (i < split.length - 1) {
                        if (!_.isObject(check)) {
                            return defaultValue;
                        }
                    }
                }
                if (_.isString(defaultValue) && _.isString(check)) return check;
                if (_.isArray(defaultValue) && _.isArray(check)) return [...check];
                if (_.isObject(defaultValue) && _.isObject(check)) return {...check};
                if (_.isNumber(defaultValue) && _.isNumber(check)) return check;
                if (_.isBoolean(defaultValue) && _.isBoolean(check)) return check;
            }
        } catch (e) {
        }
        return defaultValue;
    },
    makeStr(length = 15) {
        let text = '';
        const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < length; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    },
    sendToExtension(event, data = {}) {
        let newData = {
            event,
            data
        };
        $('#event-from-web').html(JSON.stringify(newData));
        $('#event-from-web').click();
    },
    changeAlias: function (alias) {
        var str = alias;
        str = str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, " ");
        str = str.replace(/ + /g, " ");
        str = str.trim();
        return str;
    },
    formatTime: function (time) {
        let newTime = '';
        let now = moment().format('DD/MM/YYYY');
        let yesterday = moment().subtract(1, "days").format('DD/MM/YYYY');
        let timeStr = moment(time).format('DD/MM/YYYY');
        if (now == timeStr) newTime = moment(time).format('HH:mm');
        else if (yesterday == timeStr) newTime = 'hôm qua ' + moment(time).format('HH:mm');
        else newTime = moment(time).format('DD.MM');
        return newTime;
    },
    formatStatus: function (status) {
        switch (status) {
            case -1:
                return 'Đã hủy';
            case 0:
                return 'Đơn nháp';
            case 1:
                return 'Đã xác nhận';
            case 2:
                return 'Chờ lấy hàng';
            case 3:
                return 'Đã lấy hàng';
            case 4:
                return 'Thành công';
            case 5:
                return 'Hoàn thành công';
            case 6:
                return 'Đang hoàn hàng';
            case 7:
                return 'Hoàn thành công';
            case 8:
                return 'Đề nghị hoàn';
            default:
                return 'Không xác định';
        }
    },
    playClick() {
        audio.play();
    },
    playNotify() {
        playNotify.play();
    },
    async delay(time) {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve();
            }, time);
        });
    },
    formatMoney(amount, decimalCount = 0, decimal = ",", thousands = ",") {
        if(amount[0]) {
            amount = amount.replace(/,/gi, '');
            amount = amount.replace(/\./gi, '');
        }
        try {
            decimalCount = Math.abs(decimalCount);
            decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

            const negativeSign = amount < 0 ? "-" : "";

            let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
            let j = (i.length > 3) ? i.length % 3 : 0;

            return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
        } catch (e) {
            console.log(e)
        }
    },
    showMessageResponse(dataAPI) {
        let status = this.convertVariable(dataAPI, "meta.status", 0);
        if(status == 200) {
            this.$message.success(dataAPI.meta.msg);
        } else {
            this.$message.error(dataAPI.meta.msg);
        }
    }
}
