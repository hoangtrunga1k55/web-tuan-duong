import Vue from 'vue';
import Anted from 'ant-design-vue/es';
Vue.use(Anted);

import helpers from './helpers';
Vue.mixin({
    methods: helpers
})

import CKEditor from 'ckeditor4-vue';
Vue.use(CKEditor);

Vue.component('config-language', require('./screens/ConfigLanguage/add.vue').default);
const app = new Vue({
    el: '#app'
});

