FROM thomasjackkil/php-7.3.27-fpm-alpine



RUN pecl install -o -f redis \
&&  rm -rf /tmp/pear \
&&  docker-php-ext-enable redis

RUN docker-php-ext-configure exif  && docker-php-ext-install exif && docker-php-ext-enable exif

RUN apk --no-cache update \
    && apk --no-cache upgrade \
    && apk add --no-cache $PHPIZE_DEPS \
        freetype-dev \
        libjpeg-turbo-dev \
        libpng-dev && \
    docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ --with-png-dir=/usr/include/ && \
    docker-php-ext-install -j$(getconf _NPROCESSORS_ONLN) gd

RUN pwd & ls
ADD . /code

WORKDIR /code
RUN php -d memory_limit=-1 composer.phar install

RUN php composer.phar dumpautoload

RUN pwd & ls


